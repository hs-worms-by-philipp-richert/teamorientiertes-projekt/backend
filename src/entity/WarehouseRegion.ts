import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { IWarehouseRegion } from '../shared/interfaces/IWarehouseRegion';
import { bigint } from '../helper/type-transformer.helper';
import { Warehouse } from './Warehouse';
import { WarehouseStorage } from './WarehouseStorage';

@Entity()
export class WarehouseRegion implements IWarehouseRegion {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  RegionId: bigint;

  @Column('bigint', { transformer: [bigint] })
  WarehouseId: bigint;

  @ManyToOne(() => Warehouse, (warehouse) => warehouse.WarehouseId)
  @JoinColumn({ name: 'WarehouseId' })
  Warehouse: Warehouse;

  @Column('varchar', { length: 32 })
  Name: string;

  @OneToMany(() => WarehouseStorage, (warehouseStorage) => warehouseStorage.Region)
  Storages: WarehouseStorage[];
}
