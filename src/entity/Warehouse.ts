import { Column, Entity, Generated, OneToMany, PrimaryColumn } from 'typeorm';
import { IWarehouse } from '../shared/interfaces/IWarehouse';
import { bigint } from '../helper/type-transformer.helper';
import { WarehouseRegion } from './WarehouseRegion';

@Entity()
export class Warehouse implements IWarehouse {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  WarehouseId: bigint;

  @Column('varchar', { length: 32 })
  Name: string;

  @Column('varchar', { length: 64 })
  Street: string;

  @Column('varchar', { length: 5 })
  Postal: string;

  @Column('varchar', { length: 32 })
  City: string;

  @Column('varchar', { length: 4 })
  Housenumber: string;

  @Column('varchar', { length: 16 })
  Country: string;

  @OneToMany(() => WarehouseRegion, (warehouseRegion) => warehouseRegion.Warehouse)
  Regions?: WarehouseRegion[];
}
