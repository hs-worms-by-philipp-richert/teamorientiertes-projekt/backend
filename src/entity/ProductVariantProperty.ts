import { Column, Entity, Generated, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { IProductVariantProperty } from '../shared/interfaces/IProductVariantProperty';
import { bigint } from '../helper/type-transformer.helper';
import { ProductVariant } from './ProductVariant';

@Entity()
export class ProductVariantProperty implements IProductVariantProperty {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  PropertyId: bigint;

  @ManyToOne(() => ProductVariant, (productVariant) => productVariant.VariantId)
  @JoinColumn({ name: 'VariantId' })
  @Column('bigint', { transformer: [bigint] })
  VariantId: bigint;

  @Column('varchar', { length: 32 })
  Name: string;
}
