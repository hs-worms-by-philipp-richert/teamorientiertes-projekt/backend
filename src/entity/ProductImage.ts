import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Product } from './Product';
import { IProductImage } from '../shared/interfaces/IProductImage';
import { bigint } from '../helper/type-transformer.helper';

@Entity()
export class ProductImage implements IProductImage {
  @ManyToOne(() => Product, (product) => product.ProductId)
  @JoinColumn({ name: 'ProductId' })
  @Column('bigint', { transformer: [bigint] })
  ProductId: bigint;

  @PrimaryColumn('varchar', { length: 36 })
  ImageId: string;
}
