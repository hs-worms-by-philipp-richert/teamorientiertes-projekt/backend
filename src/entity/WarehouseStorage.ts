import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { WarehouseStorageTray } from './WarehouseStorageTray';
import { IWarehouseStorage } from '../shared/interfaces/IWarehouseStorage';
import { bigint } from '../helper/type-transformer.helper';
import { WarehouseRegion } from './WarehouseRegion';

@Entity()
export class WarehouseStorage implements IWarehouseStorage {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  StorageId: bigint;

  @Column('bigint', { transformer: [bigint] })
  RegionId: bigint;

  @ManyToOne(() => WarehouseRegion, (warehouseRegion) => warehouseRegion.RegionId)
  @JoinColumn({ name: 'RegionId' })
  Region: WarehouseRegion;

  @Column('varchar', { length: 32 })
  Name: string;

  @OneToMany(() => WarehouseStorageTray, (warehouseStorageTray) => warehouseStorageTray.Storage)
  Trays: WarehouseStorageTray[];
}
