import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { ICategory } from '../shared/interfaces/ICategory';
import { bigint } from '../helper/type-transformer.helper';
import { ProductCategory } from './ProductCategory';

@Entity()
export class Category implements ICategory {
  @Column('varchar', { length: 32 })
  Name: string;

  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  CategoryId: bigint;

  @OneToMany(() => Category, (category) => category.ParentCategoryId, { nullable: true })
  ChildCategories: Category[];

  @OneToMany(() => ProductCategory, (productCategory) => productCategory.Category, { nullable: true})
  ProductCategories: ProductCategory[];

  @ManyToOne(() => Category, (category) => category.CategoryId, { nullable: true })
  @JoinColumn({ name: 'ParentCategoryId' })
  ParentCategoryId?: Category;
}
