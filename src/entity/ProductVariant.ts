import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { Product } from './Product';
import { IProductVariant } from '../shared/interfaces/IProductVariant';
import { bigint } from '../helper/type-transformer.helper';
import { ProductVariantProperty } from './ProductVariantProperty';

@Entity()
export class ProductVariant implements IProductVariant {
  @ManyToOne(() => Product, (product) => product.ProductId)
  @JoinColumn({ name: 'ProductId' })
  @Column('bigint', { transformer: [bigint] })
  ProductId: bigint;

  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  VariantId: bigint;

  @Column('varchar')
  Description: string;

  @OneToMany(() => ProductVariantProperty, (productVariantProperty) => productVariantProperty.VariantId)
  Properties: ProductVariantProperty[];
}
