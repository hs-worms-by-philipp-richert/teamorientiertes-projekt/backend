import { Column, Entity, Generated, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { WarehouseStorage } from './WarehouseStorage';
import { IWarehouseStorageTray } from '../shared/interfaces/IWarehouseStorageTray';
import { bigint } from '../helper/type-transformer.helper';

@Entity()
export class WarehouseStorageTray implements IWarehouseStorageTray {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  TrayId: bigint;
  
  @Column('bigint', { transformer: [bigint] })
  StorageId: bigint;

  @ManyToOne(() => WarehouseStorage, (warehouseStorage) => warehouseStorage.StorageId)
  @JoinColumn({ name: 'StorageId' })
  Storage: WarehouseStorage;

  @Column('varchar', { length: 32 })
  Name: string;
}
