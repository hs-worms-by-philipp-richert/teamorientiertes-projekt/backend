import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { IUserRole } from '../shared/interfaces/IUserRole';
import { Role } from './Role';
import { number } from '../helper/type-transformer.helper';
import { User } from './User';
@Entity()
export class UserRole implements IUserRole {
  @ManyToOne(() => User, (user) => user.UserId)
  @JoinColumn({ name: 'UserId' })
  @PrimaryColumn('varchar')
  UserId: string;

  @ManyToOne(() => Role, (role) => role.RoleId)
  @JoinColumn({ name: 'RoleId' })
  @PrimaryColumn('integer', { transformer: [number] })
  RoleId: number;
}
