import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { IPsaCheck } from '../shared/interfaces/IPsaCheck';
import { ProductItem } from './ProductItem';
import { bigint, dateTransformer } from '../helper/type-transformer.helper';


@Entity()
export class PsaCheck implements IPsaCheck {
  @PrimaryColumn('varchar', { length: 36 })
  PsaId: string;
  
  @Column('bigint', { transformer: [bigint] })
  ItemId: bigint;

  @ManyToOne(() => ProductItem, (productItem) => productItem.ItemId)
  @JoinColumn({ name: 'ItemId' })
  Item: ProductItem;

  @Column('timestamp', { transformer: [dateTransformer] })
  DateChecked: Date;
}