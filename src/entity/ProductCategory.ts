import { Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryColumn } from 'typeorm';
import { Product } from './Product';
import { Category } from './Category';
import { IProductCategory } from '../shared/interfaces/IProductCategory';
import { bigint } from '../helper/type-transformer.helper';

@Entity()
export class ProductCategory implements IProductCategory {
  @PrimaryColumn('bigint', { transformer: [bigint] })
  ProductId: bigint;

  @PrimaryColumn('bigint', { transformer: [bigint] })
  CategoryId: bigint;

  @ManyToOne(() => Category, (category) => category.CategoryId, { nullable: false })
  @JoinColumn({ name: 'CategoryId' })
  Category: Category;

  @ManyToOne(() => Product, (product) => product.ProductId)
  @JoinColumn({ name: 'ProductId' })
  Product: Product;
}
