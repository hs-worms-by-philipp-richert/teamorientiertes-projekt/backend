import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Product } from './Product';
import { IProductManual } from '../shared/interfaces/IProductManual';
import { bigint } from '../helper/type-transformer.helper';

@Entity()
export class ProductManual implements IProductManual {
  @ManyToOne(() => Product, (product) => product.ProductId)
  @JoinColumn({ name: 'ProductId' })
  @Column('bigint', { transformer: [bigint] })
  ProductId: bigint;

  @PrimaryColumn('varchar', { length: 36 })
  ManualId: string;

  @Column('varchar', { default: 'Manual' })
  Name: string;
}
