import { Column, Entity, Generated, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Order } from './Order';
import { ProductItem } from './ProductItem';
import { ILineItem } from '../shared/interfaces/ILineItem';
import { bigint, dateTransformer } from '../helper/type-transformer.helper';

@Entity()
export class LineItem implements ILineItem {
  @ManyToOne(() => Order, (order) => order.OrderId)
  @JoinColumn({ name: 'OrderId' })
  @Column('bigint', { transformer: [bigint] })
  OrderId: bigint;

  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  LineItemId: bigint;

  @Column('bigint', { transformer: [bigint] })
  ItemId: bigint;

  @ManyToOne(() => ProductItem, (productItem) => productItem.ItemId)
  @JoinColumn({ name: 'ItemId' })
  Item: ProductItem;

  @Column('date', { transformer: dateTransformer })
  StartDate: Date;

  @Column('date', { transformer: dateTransformer })
  EndDate: Date;

  @Column('date', { transformer: dateTransformer, nullable: true })
  ReturnDate: Date | undefined | null;
}
