import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { User } from './User';
import { LineItem } from './LineItem';
import { IOrder } from '../shared/interfaces/IOrder';
import { bigint, orderStatus } from '../helper/type-transformer.helper';
import { OrderStatus } from '../shared/enums/order-status.enum';

@Entity()
export class Order implements IOrder {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  OrderId: bigint;

  @Column('varchar')
  UserId: string;

  @ManyToOne(() => User, (user) => user.UserId)
  @JoinColumn({ name: 'UserId' })
  User: User;

  @Column('date')
  OrderDate: Date;

  @Column('integer', { transformer: orderStatus })
  OrderStatus: OrderStatus;

  @OneToMany(() => LineItem, (lineItem) => lineItem.OrderId)
  LineItems: LineItem[];
}
