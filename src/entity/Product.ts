import { Column, Entity, Generated, OneToMany, PrimaryColumn } from 'typeorm';
import { ProductVariant } from './ProductVariant';
import { ProductItem } from './ProductItem';
import { ProductImage } from './ProductImage';
import { ProductManual } from './ProductManual';
import { ProductCategory } from './ProductCategory';
import { IProduct } from '../shared/interfaces/IProduct';
import { bigint, number } from '../helper/type-transformer.helper';

@Entity()
export class Product implements IProduct {
  @Column('varchar', { length: 32 })
  Name: string;

  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  ProductId: bigint;

  @Column('boolean')
  IsInternal: boolean;

  @Column('numeric', { transformer: [number] })
  RentingFee: number;

  @Column('varchar')
  Description: string;

  @OneToMany(() => ProductVariant, (productVariant) => productVariant.ProductId)
  Variants: ProductVariant[];

  @Column('varchar', { length: 32 })
  Manufacturer: string;

  @OneToMany(() => ProductItem, (productItem) => productItem.Product)
  ProductItems: ProductItem[];

  @OneToMany(() => ProductImage, (productImage) => productImage.ProductId)
  ProductImages: ProductImage[];

  @OneToMany(() => ProductManual, (productManual) => productManual.ProductId)
  ProductManuals: ProductManual[];

  @OneToMany(() => ProductCategory, (productCategory) => productCategory.Product)
  Categories: ProductCategory[];
}
