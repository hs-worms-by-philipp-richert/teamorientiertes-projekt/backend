import { Column, Entity, Generated, PrimaryColumn } from 'typeorm';
import { ISession } from '../shared/interfaces/ISession';
import { bigint } from '../helper/type-transformer.helper';

@Entity()
export class Session implements ISession {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  Id?: bigint;

  @Column('varchar')
  UserId: string;

  @Column('varchar', { length: 36 })
  SessionId: string;

  @Column('varchar', { length: 36 })
  TokenId: string;

  @Column('bigint', { transformer: [bigint] })
  CreatedAt: bigint;
}
