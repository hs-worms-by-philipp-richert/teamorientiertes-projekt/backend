import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryColumn } from 'typeorm';
import { Product } from './Product';
import { WarehouseStorageTray } from './WarehouseStorageTray';
import { ProductVariant } from './ProductVariant';
import { IProductItem } from '../shared/interfaces/IProductItem';
import { bigint, dateTransformer, number } from '../helper/type-transformer.helper';
import { LineItem } from './LineItem';
import { IProduct } from '../shared/interfaces/IProduct';
import { PsaCheck } from './PsaCheck';

@Entity()
export class ProductItem implements IProductItem {
  @Generated('increment')
  @PrimaryColumn('bigint', { transformer: [bigint] })
  ItemId: bigint;

  @Column('varchar', { length: 32, unique: true })
  QrCode: string;

  @Column('varchar', { nullable: true })
  Serialnumber: string;

  @Column('bigint', { transformer: [bigint] })
  ProductId: bigint;

  @ManyToOne(() => Product, (product) => product.ProductId)
  @JoinColumn({ name: 'ProductId' })
  Product: Product;

  @ManyToOne(() => ProductVariant, (productVariant) => productVariant.VariantId)
  @JoinColumn({ name: 'VariantId' })
  @Column('bigint', { nullable: true, transformer: [bigint] })
  VariantId?: bigint;

  @ManyToOne(() => WarehouseStorageTray, (warehouseStorageTray) => warehouseStorageTray.TrayId, { nullable: false })
  @JoinColumn({ name: 'TrayId' })
  Tray: WarehouseStorageTray;

  @Column('bigint', { transformer: [bigint] })
  TrayId: bigint;

  @Column('date', { select: false, nullable: true, transformer: [dateTransformer] })
  ManufacturingDate: Date;

  @Column('integer', { select: false, nullable: true, transformer: [number] })
  ExpiresInYears: number;

  @Column('boolean', { select: false, default: false })
  IsPsa: boolean;

  @OneToMany(() => PsaCheck, (psaCheck) => psaCheck.Item)
  PSA: PsaCheck[];

  @Column('boolean', { select: false })
  IsDefect: boolean;

  @OneToOne(() => ProductVariant, (productVariant) => productVariant.ProductId)
  Variant: ProductVariant;

  @OneToMany(() => LineItem, (lineItem) => lineItem.Item)
  LineItems: LineItem[];

  @Column('boolean')
  Archived: boolean;
}
