import { Column, Entity, Generated, PrimaryColumn } from 'typeorm';
import { IRole } from '../shared/interfaces/IRole';
import { number, permissionTransformer } from '../helper/type-transformer.helper';
import { Permission } from '../shared/enums/permissions.enum';

@Entity()
export class Role implements IRole {
  @Generated('increment')
  @PrimaryColumn('integer', { transformer: [number] })
  RoleId: number;

  @Column('varchar', { length: 32 })
  Name: string;

  @Column('integer', { transformer: permissionTransformer })
  Permission: Permission;
}
