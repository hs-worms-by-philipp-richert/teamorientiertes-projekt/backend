import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { Order } from './Order';
import { IUser } from '../shared/interfaces/IUser';
import { number as numberTransformer } from '../helper/type-transformer.helper';
import { Role } from './Role';

@Entity()
export class User implements IUser {
  @Column('varchar', { length: 16 })
  Firstname: string;

  @Column('varchar', { length: 16 })
  Lastname: string;

  @PrimaryColumn('varchar', { length: 32 })
  UserId: string;

  @Column('varchar', { length: 64 })
  Street: string;

  @Column('varchar', { length: 5 })
  Postal: string;

  @Column('varchar', { length: 32 })
  City: string;

  @Column('varchar', { length: 4 })
  Housenumber: string;

  @Column('varchar', { length: 16 })
  Country: string;

  @Column('varchar', { length: 32, unique: true })
  Email: string;

  @Column('varchar', { length: 18 })
  Phone: string;

  @Column('boolean')
  IsVerified: boolean;

  @Column('boolean', { default: false })
  HasRequestedRoleUpdate: boolean;

  @Column('varchar', { select: false })
  Password: string;

  @OneToMany(() => Order, (order) => order.User)
  Orders: Order[];

  @Column('integer', { transformer: [numberTransformer] })
  RoleId: number;

  @ManyToOne(() => Role, (role) => role.RoleId, { nullable: false })
  @JoinColumn({ name: 'RoleId' })
  Role: Role;
}
