import Agenda from 'agenda';
import dotenv from 'dotenv';
import { OrderService } from './services/order.service';
import { EMailService } from './services/email.service';
import errorHandler from './helper/error-handler';
import { PsaService } from './services/psa.service';

dotenv.config();

const mongoConnectionString = process.env.MONGODB_CONN ?? '';

export const agenda = new Agenda({ db: { address: mongoConnectionString } });

const orderService = new OrderService();
const emailService = new EMailService();
const psaService = new PsaService();

agenda.define('orderExceeded', async function () {
  try {
    const orders = await orderService.getAllExceeded();
    for (const order of orders) {
      const emailData = {
        route: {
          path: 'email/order-exceeded',
        },
        orderData: {
          receiver: order.User!.Email,
          order: order,
        },
      };
      await emailService.createEmail(emailData.route, emailData.orderData);
    }
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    console.log(errorMsg);
  }
});

agenda.define('twoDaysLeft', async function () {
  try {
    const orders = await orderService.getAllTwoDaysRemaining();

    for (const order of orders) {
      const emailData = {
        route: {
          path: 'email/order-ends-in-two-days',
        },
        orderData: {
          receiver: order.User!.Email,
          order: order,
        },
      };
      await emailService.createEmail(emailData.route, emailData.orderData);
    }
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    console.log(errorMsg);
  }
});

agenda.define('oneDayLeft', async function () {
  try {
    const orders = await orderService.getAllOneDayRemaining();

    for (const order of orders) {
      const emailData = {
        route: {
          path: 'email/order-ends-tommorow',
        },
        orderData: {
          receiver: order.User!.Email,
          order: order,
        },
      };

      await emailService.createEmail(emailData.route, emailData.orderData);
    }
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    console.log(errorMsg);
  }
});

agenda.define('noDaysLeft', async function () {
  try {
    const orders = await orderService.getAllNoDayRemaining();

    for (const order of orders) {
      const emailData = {
        route: {
          path: 'email/order-ends-today',
        },
        orderData: {
          receiver: order.User!.Email,
          order: order,
        },
      };
      await emailService.createEmail(emailData.route, emailData.orderData);
    }
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    console.log(errorMsg);
  }
});

agenda.define('monthlyPSAReport', async function () {
  try {
    const resultsNextMonth = await psaService.nextMonthExpiringPSAs();
    const resultsAlreadyExpired = await psaService.expiredPSAs();

    const emailData = {
      route: {
        path: 'email/psa-overview',
      },
      data: {
        psaExpiringSoon: resultsNextMonth,
        psaAlreadyExpired: resultsAlreadyExpired,
      },
    };

    await emailService.createEmail(emailData.route, emailData.data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    console.log(errorMsg);
  }
});

agenda.define('dailyOrderReport', async function() {
  try {
    const orders = await orderService.todaysOrders();
    console.log(orders);
    if(!(orders.length > 0)) return;

    const emailData = {
      route: {
        path: 'email/orders-of-last-24-hours',
      },
      orderData: {
        order: orders,
      },
    };

    await emailService.createEmail(emailData.route, emailData.orderData);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    console.log(errorMsg)
  }
});