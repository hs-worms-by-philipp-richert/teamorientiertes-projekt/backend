import express, { Application, NextFunction, Request, Response } from 'express';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import cors from 'cors';
import { AppDataSource } from './data-source';
import indexRouter from './routes/index.router';
import { agenda } from './agenda-jobs';
import { IUser } from './shared/interfaces/IUser';
import helmet from 'helmet';

dotenv.config();

declare module 'express-serve-static-core' {
  export interface Request {
    user?: IUser;
  }
}

declare module 'http' {
  interface IncomingHttpHeaders {
    'x-refresh-semaphore'?: string;
  }
}


const app: Application = express();

app.use(helmet({ crossOriginResourcePolicy: { policy: 'cross-origin' }}))
app.use(cors({
  credentials: true,
  origin: process.env.FRONTEND_ORIGIN!,
  exposedHeaders: ['set-cookie', 'x-set-access-token']
}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
const port = process.env.PORT || 3000;

AppDataSource.initialize()
  .then(() => {
    if (process.env.NODE_ENV !== 'production') console.log('Data Source has been initialized!');
  })
  .catch((error) => console.error('Error during Data Source initialization:', error));

if (process.env.NODE_ENV === 'production') {
  console.log('The application is running in production-mode');
} else {
  console.log('The application is running in development-mode');
}

app.use('/', indexRouter);

app.get('/', (req: Request, res: Response) => {
  res.send('Working');
});

app.listen(port, () => {
  console.log(`Server 'backend' is running at http://localhost:${port}`);
});

// agenda jobs
(async function () {
  if (process.env.NODE_ENV !== 'production') return;

  const standardCron = '0 0 8 * * *'; //every day at 8:00am
  const firstDayOfMonth = '0 0 6 1 * *'; //every 1st day at 6:00

  await agenda.start();
  await agenda.every(standardCron, 'twoDaysLeft');
  await agenda.every(standardCron, 'oneDayLeft');
  await agenda.every(standardCron, 'noDaysLeft');

  await agenda.every(standardCron, 'orderExceeded');
  await agenda.every('0 0 6 * * *', 'dailyOrderReport');
  await agenda.every(firstDayOfMonth, 'monthlyPSAReport');
})();
