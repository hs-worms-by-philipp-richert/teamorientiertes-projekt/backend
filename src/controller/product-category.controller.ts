import { Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ErrorType } from '../shared/enums/error-types.enum';
import { ProductCategoryService } from '../services/product-category.service';
import { IProductCategory } from '../shared/interfaces/IProductCategory';
import validationHelper from '../helper/validation-helper';

const productCategoryService = new ProductCategoryService();

export const createProductCategory = async (req: Request, res: Response) => {
  try {
    const data = req.body as IProductCategory;

    if (
      !data ||
      !validationHelper.validateBigInt(data?.CategoryId) ||
      !validationHelper.validateBigInt(data?.ProductId)
    )
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    await productCategoryService.create(data);

    return res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteAllProductCategoriesById = async (req: Request, res: Response) => {
  try {
    const { productId } = req.params;

    if (!productId || !validationHelper.validateBigInt(productId))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(productId);

    const data = {
      productId: parsedId,
      categoryId: BigInt(0),
      transactionalEntityManager: null,
    };

    await productCategoryService.deleteAllById(data);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteAllProductCategoriesByCategoryId = async (req: Request, res: Response) => {
  try {
    const { categoryId } = req.params;
    if (!categoryId || !validationHelper.validateBigInt(categoryId))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(categoryId);

    const data = {
      productId: parsedId,
      categoryId: BigInt(0),
      transactionalEntityManager: null,
    };

    await productCategoryService.deleteAllById(data);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteByIds = async (req: Request, res: Response) => {
  try {
    const { ProductId: productId, CategoryId: categoryId } = req.body;

    if (
      !productId ||
      !categoryId ||
      !validationHelper.validateBigInt(productId) ||
      !validationHelper.validateBigInt(categoryId)
    )
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedProductId = BigInt(productId);
    const parsedCategoryId = BigInt(categoryId);

    await productCategoryService.deleteOneByIds(parsedProductId, parsedCategoryId);

    res.sendStatus(HttpCode.OK);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
