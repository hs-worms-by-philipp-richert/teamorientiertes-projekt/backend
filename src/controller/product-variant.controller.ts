import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { ProductVariantService } from '../services/product-variant.service';
import { IProductVariant } from '../shared/interfaces/IProductVariant';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ErrorType } from '../shared/enums/error-types.enum';

const productVariantService = new ProductVariantService();

export const createProductVariant = async (req: Request, res: Response) => {
  try {
    const data = req.body as IProductVariant;

    if (!data.ProductId || !data.Description)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    await productVariantService.create(data);
    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getAllByProductId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const parsedId = BigInt(id);

    const productVariants = productVariantService.getAllByProductId(parsedId);
    res.status(HttpCode.Accepted).json(productVariants);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getAllByVariantId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const parsedId = BigInt(id);

    const productVariants = await productVariantService.readById(parsedId);
    res.status(HttpCode.Accepted).json(productVariants);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateProductVariantById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { Description: description } = req.body as IProductVariant;

    if (!id || !description)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const parsedId = BigInt(id);

    await productVariantService.patchById(parsedId, description);
    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteProductVariantById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const parsedId = BigInt(id);

    await productVariantService.deleteById(parsedId);
    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

//TO DO
// export const deleteVariantByProductId = async (
//   req: Request,
//   res: Response
// ) => {
//   try {
//     const { id } = req.params;

//     if (!id)
//       return errorHandler.handleControllerCancellation(
//         res,
//         HttpCode.BadRequest,
//         ErrorType.InvalidInput
//       );

//     const parsedId = BigInt(id);

//     await productVariantService.deleteByProductId(parsedId);
//     res.sendStatus(HttpCode.Accepted);
//   } catch (error) {
//     const errorMsg: string = errorHandler.handleControllerException(error);
//     res.status(HttpCode.InternalServerError).json({ error: errorMsg });
//   }
// };
