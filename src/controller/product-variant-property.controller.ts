import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { ProductVariantPropertyService } from '../services/product-variant-property.service';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IProductVariantProperty } from '../shared/interfaces/IProductVariantProperty';
import validationHelper from '../helper/validation-helper';

const productVariantPropertyService = new ProductVariantPropertyService();

export const createVariantProperty = async (req: Request, res: Response) => {
  try {
    const data = req.body as IProductVariantProperty;

    if (!data.VariantId || !data.Name)
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    await productVariantPropertyService.create(data);
    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getAllByVariantId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || validationHelper.validateBigInt(id))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);
    const productVariants = productVariantPropertyService.getAllByVariantId(parsedId);

    res.status(HttpCode.Accepted).json(productVariants);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateVariantPropertyByPropertyId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { Name: name } = req.body;

    if (!id || validationHelper.validateBigInt(id) || !name)
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);
    await productVariantPropertyService.patchById(parsedId, name);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteVariantPropertyById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || validationHelper.validateBigInt(id))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);
    await productVariantPropertyService.deleteById(parsedId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
