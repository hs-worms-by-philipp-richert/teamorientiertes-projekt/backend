import { Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { LineItemService } from '../services/line-item.service';
import { ErrorType } from '../shared/enums/error-types.enum';
import { ILineItem } from '../shared/interfaces/ILineItem';
import validationHelper from '../helper/validation-helper';
import { DateComparison } from '../shared/enums/date-comparison.enum';

const lineItemService = new LineItemService();

export const createLineItem = async (req: Request, res: Response) => {
  try {
    const data = req.body as ILineItem;

    if (
      !data.OrderId ||
      !data.ItemId ||
      !data.StartDate ||
      !data.EndDate ||
      data.EndDate < data.StartDate ||
      data.ReturnDate !== undefined
    ) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    await lineItemService.create(data);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateLineItemById = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    let { returnDate } = req.body;

    if (
      !id ||
      !returnDate ||
      !validationHelper.validateDate(returnDate, DateComparison.TodayAndFuture) ||
      !validationHelper.validateBigInt(id.toString())
    ) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId = BigInt(id);

    (await lineItemService.putById(parsedId, returnDate))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const setReturnDateById = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;

    if (!id || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId = BigInt(id);

    (await lineItemService.setReturnDateById(parsedId, new Date()))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const setReturnDateToNullById = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;

    if (!id || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId = BigInt(id);

    (await lineItemService.setReturnDateById(parsedId, null))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteLineItemById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId = BigInt(id);

    await lineItemService.deleteById(parsedId);

    res.status(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
