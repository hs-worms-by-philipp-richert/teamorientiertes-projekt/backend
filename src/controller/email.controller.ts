import { Request, Response } from 'express';
import validationHelper from '../helper/validation-helper';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ErrorType } from '../shared/enums/error-types.enum';
import { OrderService } from '../services/order.service';
import { EMailService } from '../services/email.service';
import { IOrder } from '../shared/interfaces/IOrder';
import { PsaService } from '../services/psa.service';

const orderService = new OrderService();
const emailService = new EMailService();
const psaService = new PsaService();

export const createOrderInvoice = async (req: Request, res: Response) => {
  try {
    const { orderId } = req.params;

    if (!orderId || !validationHelper.validateBigInt(orderId.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedOrderId = BigInt(orderId);

    const dataOrder: IOrder | null = await orderService.readById(parsedOrderId);

    if (!dataOrder) return res.sendStatus(HttpCode.NotFound);

    const emailData = {
      route: {
        path: 'email/order-invoice',
      },
      data: {
        receiver: dataOrder.User!.Email,
        order: dataOrder,
      },
    };
    await emailService.createEmail(emailData.route, emailData.data);
    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createReadyForPickup = async (req: Request, res: Response) => {
  try {
    const { orderId } = req.params;

    if (!orderId || !validationHelper.validateBigInt(orderId.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedOrderId = BigInt(orderId);

    const dataOrder: IOrder | null = await orderService.readById(parsedOrderId);

    if (!dataOrder) return res.sendStatus(HttpCode.NotFound);

    const emailData = {
      route: {
        path: 'email/ready-for-pickup',
      },
      data: {
        receiver: dataOrder.User!.Email,
        order: dataOrder,
      },
    };

    await emailService.createEmail(emailData.route, emailData.data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const orderExceeded = async (req: Request, res: Response) => {
  try {
    const { orderId } = req.params;

    if (!orderId || !validationHelper.validateBigInt(orderId.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedOrderId = BigInt(orderId);

    const dataOrder: IOrder | null = await orderService.readById(parsedOrderId);

    if (!dataOrder) return res.sendStatus(HttpCode.NotFound);

    const emailData = {
      route: {
        path: 'email/order-exceeded',
      },
      data: {
        receiver: dataOrder.User!.Email,
        order: dataOrder,
      },
    };

    await emailService.createEmail(emailData.route, emailData.data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};