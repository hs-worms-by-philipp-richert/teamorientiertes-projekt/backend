import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ProductItemService } from '../services/product-item.service';
import { IProductItem } from '../shared/interfaces/IProductItem';
import validationHelper from '../helper/validation-helper';
import { ErrorType } from '../shared/enums/error-types.enum';

const productItemService = new ProductItemService();

export const getProductItemById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const itemId = req.params.id;

    if (!itemId || !validationHelper.validateBigInt(itemId)) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }
    const productItem = await productItemService.readById(BigInt(itemId));

    if (!productItem) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(productItem);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getProductItemsByProductId: RequestHandler = async (req: Request, res: Response) => {
  try {
    const productId = req.params.id;

    if (!productId || !validationHelper.validateBigInt(productId)) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const productItem = await productItemService.getAllByProductId(BigInt(productId));

    if (productItem.length <= 0) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(productItem);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createProductItem: RequestHandler = async (req: Request, res: Response) => {
  try {
    let data = req.body as IProductItem;
    data.IsDefect = false;

    if (!data.QrCode || !data.ProductId) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }
    
    await productItemService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateProductItemById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const itemId = req.params.id;
    const data = req.body as IProductItem;

    if (!itemId || data.Archived || !validationHelper.validateBigInt(itemId))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    (await productItemService.putById(BigInt(itemId), data))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteProductItemById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const itemId = req.params.id;

    if (!itemId || !validationHelper.validateBigInt(itemId))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    (await productItemService.deleteById(BigInt(itemId)))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
