import { Request, RequestHandler, Response } from "express";
import { RoleService } from "../services/role.service";
import { HttpCode } from "../shared/enums/http-codes.enum";
import { ErrorType } from "../shared/enums/error-types.enum";

const roleService: RoleService = new RoleService();

export const getAllRoles: RequestHandler = async (req: Request, res: Response) => {
  const list = await roleService.list();

  if (list.total < 0) {
    return res.status(HttpCode.NotFound).json({ error: ErrorType.NotFound });
  }

  res.status(HttpCode.OK).json(list.data);
};
