import { RequestHandler, Request, Response, response } from 'express';
import errorHandler from '../helper/error-handler';
import WarehouseService from '../services/warehouse.service';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { Warehouse } from '../entity/Warehouse';
import { PagedList } from '../shared/models/paged-list.model';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IWarehouse } from '../shared/interfaces/IWarehouse';
import validationHelper from '../helper/validation-helper';

const warehouseService = new WarehouseService();

export const getWarehousesList: RequestHandler = async (req: Request, res: Response) => {
  try {
    let { page = '1', take = '30' } = req.query;

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedPage: number = parseInt(page.toString());
    const parsedTake: number = parseInt(take.toString());

    const list = await warehouseService.list(parsedTake, parsedPage);

    const data: PagedList<Warehouse[]> = {
      currentPage: Number(page),
      list: list.data,
      totalLength: list.totalLength,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getWarehouseByID = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    const foundWarehouse = await warehouseService.readById(parsedId);

    if (!foundWarehouse) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(foundWarehouse);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createWarehouse: RequestHandler = async (req: Request, res: Response) => {
  try {
    let data: IWarehouse = req.body as IWarehouse;

    if (!data.Name || !data.Street || !data.Postal || !data.City || !data.Housenumber || !data.Country) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    await warehouseService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateWarehouseById: RequestHandler = async (req, res) => {
  try {
    const { id } = req.params;

    let data: IWarehouse = req.body as IWarehouse;

    if (
      !id ||
      !validationHelper.validateBigInt(id.toString()) ||
      !data.Name ||
      !data.City ||
      !data.Country ||
      !data.Housenumber ||
      !data.Postal ||
      !data.Street
    ) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId: bigint = BigInt(id);

    data.WarehouseId = parsedId;

    (await warehouseService.putById(data)) ? res.sendStatus(HttpCode.Accepted) : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteById: RequestHandler = async (req, res) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    await warehouseService.deleteById(parsedId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
