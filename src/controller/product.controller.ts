import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { ProductService } from '../services/product.service';
import { PagedAvailabilityList, PagedList } from '../shared/models/paged-list.model';
import { Product } from '../entity/Product';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ProductListFilter } from '../shared/models/product-list-filter.model';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IProduct } from '../shared/interfaces/IProduct';
import validationHelper from '../helper/validation-helper';
import { DateComparison } from '../shared/enums/date-comparison.enum';
import { Permission } from '../shared/enums/permissions.enum';
import AuthService from '../services/auth.service';

const productService = new ProductService();

export const getProductList: RequestHandler = async (req: Request, res: Response) => {
  try {
    let { search, categories, sortBy, sort, manufacturer, startDate, endDate, page = '1', take = '30' } = req.query;
    categories = categories as string;

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const isInternal = AuthService.isAllowedTo(
      Permission.InternalMember,
      AuthService.parsePermissions(req.user?.Role.Permission),
    );

    sort = sort?.toString().toUpperCase() as 'ASC' | 'DESC' | undefined;
    const filterOptions: ProductListFilter = {
      search: (search ?? '').toString(),
      categories: categories ? categories.split(',') : ([] as Array<string>),
      manufacturer: (manufacturer ?? '').toString(),
      sort: sort ?? 'ASC',
      sortBy: sortBy?.toString() === 'RentingFee' ? sortBy.toString() : 'Name',
      isInternal: isInternal,
      startDate: startDate?.toString() ?? '',
      endDate: endDate?.toString() ?? '',
    };

    if (startDate && !validationHelper.validateDate(filterOptions.startDate, DateComparison.TodayAndFuture))
      return res.sendStatus(HttpCode.BadRequest);

    if (endDate && !validationHelper.validateDate(filterOptions.endDate, DateComparison.TodayAndFuture))
      return res.sendStatus(HttpCode.BadRequest);

    if (
      startDate &&
      endDate &&
      new Date(filterOptions.startDate).getTime() >= new Date(filterOptions.endDate).getTime()
    ) {
      return res.sendStatus(HttpCode.BadRequest);
    }

    if (filterOptions.categories.length > 0) {
      try {
        for (const categoryId of filterOptions.categories) BigInt(categoryId);
      } catch {
        return res.sendStatus(HttpCode.BadRequest);
      }
    }

    const list = await productService.listFiltered(Number(take), Number(page), filterOptions);

    const data: PagedAvailabilityList<Product[]> = {
      currentPage: list.page,
      totalAvailable: list.totalAvailable,
      totalUnavailable: list.totalUnavailable,
      listAvailable: list.dataAvailable,
      listUnavailable: list.dataUnavailable,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getProductById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const productId: bigint = BigInt(req.params.id);

    const product: Product | null = await productService.readById(productId);

    if (!product) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(product);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getProductAvailability: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { productId: productIdString, startDate: startDateString, endDate: endDateString } = req.params;

    if (!validationHelper.validateBigInt(productIdString)) return res.sendStatus(HttpCode.BadRequest);

    if (!validationHelper.validateDate(startDateString, DateComparison.TodayAndFuture))
      return res.sendStatus(HttpCode.BadRequest);

    if (!validationHelper.validateDate(endDateString, DateComparison.TodayAndFuture))
      return res.sendStatus(HttpCode.BadRequest);

    if (new Date(startDateString).getTime() >= new Date(endDateString).getTime())
      return res.sendStatus(HttpCode.BadRequest);

    const productId = BigInt(productIdString);
    const startDate = new Date(startDateString);
    const endDate = new Date(endDateString);

    const { product, availableItems, unavailableItems } = await productService.listAvailableItemsByProductId(
      productId,
      startDate,
      endDate,
    );

    if (!product) return res.sendStatus(HttpCode.NotFound);

    res.json({
      productId: product.ProductId,
      name: product.Name,
      availableItems,
      unavailableItems,
    });
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createProduct = async (req: Request, res: Response) => {
  try {
    const data = req.body as IProduct;

    const parsedRentingFee = Number(data.RentingFee);

    if (
      !data.Name ||
      data.IsInternal === null ||
      data.RentingFee === null ||
      data.RentingFee === undefined ||
      isNaN(parsedRentingFee) ||
      !data.Description ||
      !data.Manufacturer
    ) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    data.RentingFee = parsedRentingFee;

    const productId = await productService.create(data);

    return res.status(HttpCode.Accepted).json({ ProductId: productId });
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateProductById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = req.body as IProduct;
    const parsedRentingFee = Number(data.RentingFee);

    if (
      !id ||
      !data.Name ||
      data.IsInternal === null ||
      data.RentingFee === null ||
      data.RentingFee === undefined ||
      isNaN(parsedRentingFee) ||
      !data.Description ||
      !data.Manufacturer
    ) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId = BigInt(id);

    (await productService.putById(parsedId, data))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteProductById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    await productService.deleteById(parsedId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
