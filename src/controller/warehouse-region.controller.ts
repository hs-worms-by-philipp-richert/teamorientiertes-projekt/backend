import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { PagedList } from '../shared/models/paged-list.model';
import { ErrorType } from '../shared/enums/error-types.enum';
import WarehouseRegionService from '../services/warehouse-region.service';
import { WarehouseRegion } from '../entity/WarehouseRegion';
import { IWarehouseRegion } from '../shared/interfaces/IWarehouseRegion';
import WarehouseService from '../services/warehouse.service';
import validationHelper from '../helper/validation-helper';

const warehouseRegionService = new WarehouseRegionService();
const warehouseSevice = new WarehouseService();

export const getRegionList: RequestHandler = async (req: Request, res: Response) => {
  try {
    let { page = '1', take = '30' } = req.query;

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedPage: number = parseInt(page.toString());
    const parsedTake: number = parseInt(take.toString());

    const list = await warehouseRegionService.list(parsedTake, parsedPage);

    const data: PagedList<WarehouseRegion[]> = {
      currentPage: Number(page),
      list: list.data,
      totalLength: list.totalLength,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getRegionById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    const foundRegion = await warehouseRegionService.readById(parsedId);

    if (!foundRegion) {
      return res.sendStatus(HttpCode.NotFound);
    }

    res.status(HttpCode.OK).json(foundRegion);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getRegionsByWarehouseId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    const foundRegions = await warehouseRegionService.readAllByWarehouseId(parsedId);

    if (!foundRegions) return res.sendStatus(HttpCode.NotFound);

    return res.status(HttpCode.OK).json(foundRegions);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createRegion: RequestHandler = async (req: Request, res: Response) => {
  try {
    let data = req.body as IWarehouseRegion;

    const { Name, WarehouseId } = data;

    if (!Name || !WarehouseId)
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(WarehouseId);
    data.WarehouseId = parsedId;

    await warehouseRegionService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateRegionById: RequestHandler = async (req, res) => {
  try {
    const { id } = req.params;

    let data = req.body as IWarehouseRegion;

    if (!id || !validationHelper.validateBigInt(id.toString()) || !data.WarehouseId) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId: bigint = BigInt(id);

    const foundWarehouse = await warehouseSevice.readById(data.WarehouseId);

    if (!foundWarehouse) return res.sendStatus(HttpCode.NotFound);

    (await warehouseRegionService.putById(parsedId, data))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteRegionById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId: bigint = BigInt(id);
    const foundRegion = await warehouseRegionService.readById(parsedId);

    if (!foundRegion) return res.sendStatus(HttpCode.NotFound);

    await warehouseRegionService.deleteById(parsedId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
