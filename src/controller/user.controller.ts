import { RequestHandler, Request, Response } from 'express';
import { UserService } from '../services/user.service';
import { HttpCode } from '../shared/enums/http-codes.enum';
import errorHandler from '../helper/error-handler';
import { IUser } from '../shared/interfaces/IUser';
import validationHelper from '../helper/validation-helper';
import { ErrorType } from '../shared/enums/error-types.enum';
import authService from '../services/auth.service';
import RegExValidation from '../helper/regex-validation';
import { EMailService } from '../services/email.service';

const userService = new UserService();
const emailService = new EMailService();

// export const setMaterialOrderAcknowledgement: RequestHandler = async (
//   req: Request,
//   res: Response
// ) => {
//   try {
//     // TODO: replace this line with req.user.id when JWT has been implemented
//     const user = await userService.readById(req.params.id);

//     if (!user) {
//       return res.status(HttpCode.BadRequest).json({ error: 'User not found.' });
//     }

//     await userService.setMaterialOrderAcknowledgement(user.UserId);
//     res.status(HttpCode.Accepted);
//   } catch (error) {
//     const errorMsg: string = errorHandler.handleControllerException(error);
//     res.status(HttpCode.InternalServerError).json({ error: errorMsg });
//   }
// };

export const postRegistration: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userData = req.body as IUser;
    const { randomPassword } = req.query;

    if (!RegExValidation.validate(userData.UserId, RegExValidation.userIdExp)) {
      return res.status(HttpCode.BadRequest).json({ error: ErrorType.InvalidUserId });
    }

    if (randomPassword == 'true') {
      userData.Password = authService.randomPasswordGenerator();
    }

    const checkProperties = [
      'Firstname',
      'Lastname',
      'UserId',
      'Street',
      'Postal',
      'City',
      'Housenumber',
      'Country',
      'Email',
      'Phone',
      'Password',
    ];

    if (validationHelper.checkPropertiesNullOrEmpty<IUser>(userData, checkProperties)) {
      return res.status(HttpCode.BadRequest).json({ error: ErrorType.InvalidInput });
    }

    const result = await userService.create(userData);

    if (result != undefined) {
      return res.status(HttpCode.BadRequest).json({ error: result });
    }

    // if (randomPassword == 'true') {
    //   // send email to user with password
    // }

    delete userData.Password;

    const emailData = {
      route: {
        path: 'email/welcome-new-user',
      },
      data: {
        receiver: userData.Email,
        user: userData,
      },
    };

    if (process.env.NODE_ENV === 'production') {
      await emailService.createEmail(emailData.route, emailData.data);
    }

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
