import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { PagedList } from '../shared/models/paged-list.model';
import { Category } from '../entity/Category';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ICategory } from '../shared/interfaces/ICategory';
import { ErrorType } from '../shared/enums/error-types.enum';
import { CategoryService } from '../services/category.service';
import validationHelper from '../helper/validation-helper';

const categoryService = new CategoryService();

export const getFilteredCategoryList: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { page, take } = req.query;
    const { showEmpty = 'false', cascade = 'false' } = req.query;
    const showEmptyCategory = showEmpty.toString() === 'true' ? true : false;
    const cascadeSubcategory = cascade.toString() === 'true' ? true : false;

    const paramError = errorHandler.handleControllerListParameters(page?.toString() ?? '1', take?.toString() ?? '30');

    if (paramError) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedPage: number = parseInt(page?.toString() ?? '0');
    const parsedTake: number = parseInt(take?.toString() ?? '0');

    const list = await categoryService.listFiltered(parsedTake, parsedPage, { showEmptyCategory, cascadeSubcategory });

    const data: PagedList<Category[]> = {
      currentPage: Number(page),
      totalLength: list.total,
      list: list.data,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getCategoryById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const categoryId = req.params.id;

    if (!categoryId || !validationHelper.validateBigInt(categoryId.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedCategoryId = BigInt(categoryId);

    const { page = '1', take = '30', extended = 'false' } = req.query;

    if (extended == 'false') {
      const category: Category | null = await categoryService.readById(parsedCategoryId);

      if (!category) return res.sendStatus(HttpCode.NotFound);

      res.json(category);
    } else if (extended == 'true') {
      const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

      if (paramError)
        return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

      const categoryInfos = await categoryService.getCategoryProductsById(parsedCategoryId, Number(take), Number(page));

      if (!categoryInfos.category) {
        return res.sendStatus(HttpCode.NotFound);
      }

      res.status(HttpCode.OK).json(categoryInfos);
    } else {
      res.sendStatus(HttpCode.BadRequest);
    }
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createCategory = async (req: Request, res: Response) => {
  try {
    const data = req.body as ICategory;

    if (!data.Name) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    await categoryService.create(data);

    return res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateCategoryById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = req.body as ICategory;

    if (!id || !validationHelper.validateBigInt(id.toString()) || !data.Name) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId = BigInt(id);

    (await categoryService.putById(parsedId, data))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteCategoryById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    await categoryService.deleteById(parsedId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
