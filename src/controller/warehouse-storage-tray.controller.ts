import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { PagedList } from '../shared/models/paged-list.model';
import { ErrorType } from '../shared/enums/error-types.enum';
import WarehouseStorageTrayService from '../services/warehouse-storage-tray.service';
import WarehouseStorageService from '../services/warehouse-storage.service';
import { IWarehouseStorageTray } from '../shared/interfaces/IWarehouseStorageTray';
import { WarehouseStorageTray } from '../entity/WarehouseStorageTray';
import validationHelper from '../helper/validation-helper';

const warehouseStorageTrayService = new WarehouseStorageTrayService();
const warehouseStorageSevice = new WarehouseStorageService();

export const getTrayList: RequestHandler = async (req: Request, res: Response) => {
  try {
    let { page = '1', take = '30' } = req.query;

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedPage: number = parseInt(page.toString());
    const parsedTake: number = parseInt(take.toString());

    const list = await warehouseStorageTrayService.list(parsedTake, parsedPage);

    const data: PagedList<WarehouseStorageTray[]> = {
      currentPage: Number(page),
      list: list.data,
      totalLength: list.totalLength,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getTrayById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    const foundStorageTray = await warehouseStorageTrayService.readById(parsedId);

    if (!foundStorageTray) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(foundStorageTray);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getTraysByStorageId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id.toString());

    const foundStorageTrays = await warehouseStorageTrayService.readAllByStorageId(parsedId);

    if (!foundStorageTrays) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(foundStorageTrays);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createTray: RequestHandler = async (req: Request, res: Response) => {
  try {
    let data = req.body as IWarehouseStorageTray;

    const { Name, StorageId } = data;

    if (!Name || !StorageId)
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedStorageId = BigInt(StorageId);

    const foundStorage = await warehouseStorageSevice.readById(parsedStorageId);

    if (!foundStorage) return res.sendStatus(HttpCode.NotFound);

    data.StorageId = parsedStorageId;

    await warehouseStorageTrayService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateTrayById: RequestHandler = async (req, res) => {
  try {
    const { id } = req.params;

    let data = req.body as IWarehouseStorageTray;

    if (!id || !data.StorageId || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedStorageTrayId: bigint = BigInt(id);

    const foundStorageTray = await warehouseStorageTrayService.readById(parsedStorageTrayId);

    if (!foundStorageTray) return res.sendStatus(HttpCode.NotFound);

    (await warehouseStorageTrayService.putById(parsedStorageTrayId, data))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteTrayById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId: bigint = BigInt(id);
    const foundTray = await warehouseStorageTrayService.readById(parsedId);

    if (!foundTray) return res.sendStatus(HttpCode.NotFound);

    await warehouseStorageTrayService.deleteById(parsedId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
