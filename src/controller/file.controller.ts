import { RequestHandler, Request, Response, NextFunction } from 'express';
import { HttpCode } from '../shared/enums/http-codes.enum';
import errorHandler from '../helper/error-handler';
import fs from 'fs';
import FileService from '../services/file.service';
import { IProductImage } from '../shared/interfaces/IProductImage';
import { IProductManual } from '../shared/interfaces/IProductManual';
import multer, { MulterError } from 'multer';
import { storage } from '../helper/multer-config.helper';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IPsaCheck } from '../shared/interfaces/IPsaCheck';
import { ProductService } from '../services/product.service';
import { ProductItemService } from '../services/product-item.service';
import validationHelper from '../helper/validation-helper';
const upload = multer({ storage });

const fileService = new FileService();
const productService = new ProductService();
const productItemService = new ProductItemService();
const uploadFileMiddleware = upload.single('file');

export const uploadPreconditionMiddleware: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { uploadType, parentIdentifier } = req.params;

    if (!validationHelper.validateBigInt(parentIdentifier)) {
      return res.sendStatus(HttpCode.BadRequest);
    }

    const parentId: bigint = BigInt(parentIdentifier);
    
    if ((uploadType == 'product-image' || uploadType == 'product-manual') && !(await productService.readById(parentId))) {
      return res.status(HttpCode.BadRequest).json({ error: ErrorType.ForeignKeyNotFound });
    }

    if (uploadType == 'psa-check' && !(await productItemService.readById(parentId))) {
      return res.status(HttpCode.BadRequest).json({ error: ErrorType.ForeignKeyNotFound });
    }

    next();
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const multerErrorHandlingMiddleware: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    uploadFileMiddleware(req, res, (err) => {
      if (err instanceof MulterError) {
        return res.status(HttpCode.BadRequest).json({ error: err.field });
      } else if (err) {
        const errorMsg: string = errorHandler.handleControllerException(err);
        return res.status(HttpCode.InternalServerError).json({ error: errorMsg });
      }

      next();
    });
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const uploadFile: RequestHandler = async (req: Request, res: Response) => {
  try {
    const filename = req.file?.filename;
    const originalname = req.file?.originalname?.split('.')[0];
    const { uploadType, parentIdentifier } = req.params;

    if (!filename) {
      return res.sendStatus(HttpCode.BadRequest);
    }

    const parentId: bigint = BigInt(parentIdentifier);

    const resource: any = {
      ProductId: parentId,
    };

    if (uploadType === 'product-image') {
      resource.ImageId = filename;

      await fileService.create(resource as IProductImage);
    } else if (uploadType === 'product-manual') {
      resource.ManualId = filename;
      resource.Name = originalname ?? filename;

      await fileService.create(resource as IProductManual);
    } else if (uploadType === 'psa-check') {
      const psaCheck: IPsaCheck = {
        PsaId: filename,
        ItemId: parentId,
        DateChecked: new Date()
      };
      await fileService.create(psaCheck);
    }

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getFile: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { fileType, parentIdentifier, fileId } = req.params;

    if (!fs.existsSync(`./uploads/${fileType}/${parentIdentifier}/${fileId}`)) {
      return res.sendStatus(HttpCode.NotFound);
    }

    switch (fileType) {
      case 'product-image':
        res.set('Content-Type', 'image/jpeg');
        break;
      case 'product-manual':
      case 'psa-check':
        res.set('Content-Type', 'application/pdf');
        break;
    }

    res.sendFile(`${fileId}`, { root: `./uploads/${fileType}/${parentIdentifier}` });
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteFilesByProductId = async (req: Request, res: Response) => {
  try {
    const { id, fileType } = req.params;

    if (!id || !fileType) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const fileTypeString = fileType.toString();
    const parsedId = BigInt(id);

    const found = await fileService.getFilesByProductId(parsedId, fileType);

    if (!found) {
      return errorHandler.handleControllerCancellation(res, HttpCode.NotFound, ErrorType.InvalidInput);
    } else if (!(found.length > 0)) {
      return errorHandler.handleControllerCancellation(res, HttpCode.NotFound, ErrorType.InvalidInput);
    }

    await fileService.deleteDirectoryByIdentifier(parsedId, fileTypeString);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteFileById = async (req: Request, res: Response) => {
  try {
    const { fileId, fileType, parentIdentifier } = req.params;

    if (!fileId || !fileType || !parentIdentifier) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    if (!validationHelper.validateBigInt(parentIdentifier)) {
      return res.sendStatus(HttpCode.BadRequest);
    }

    const parentId: bigint = BigInt(parentIdentifier);

    if (!fs.existsSync(`./uploads/${fileType}/${parentIdentifier}/${fileId}`)) {
      return res.sendStatus(HttpCode.NotFound);
    }

    await fileService.deleteFileById(fileId, fileType, parentId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
