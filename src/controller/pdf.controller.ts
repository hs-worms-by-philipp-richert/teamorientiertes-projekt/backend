import { Request, RequestHandler, Response } from 'express';
import { PDFGeneratorService } from '../services/pdf.service';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { OrderService } from '../services/order.service';
import validationHelper from '../helper/validation-helper';
import errorHandler from '../helper/error-handler';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IOrder } from '../shared/interfaces/IOrder';
import fs from 'fs/promises';
import path from 'path';

const pdfGeneratorService = new PDFGeneratorService();
const orderService = new OrderService();

export const generatePDF: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { orderId } = req.params;

    if (!orderId || !validationHelper.validateBigInt(orderId.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }
    const parsedOrderId = BigInt(orderId);

    const dataOrder: IOrder | null = await orderService.readById(parsedOrderId);

    if (!dataOrder) return errorHandler.handleControllerCancellation(res, HttpCode.NotFound, ErrorType.InvalidInput);

    const created = await pdfGeneratorService.create(dataOrder);
    if (!created) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getPDF: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { orderId } = req.params;

    if (!orderId || !validationHelper.validateBigInt(orderId))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedOrderId = BigInt(orderId);
    const results = await pdfGeneratorService.readPDF(parsedOrderId);

    if (!results) return res.sendStatus(HttpCode.NotFound);

    res.set('Content-Type', 'application/pdf');
    res.sendFile(`${results.orderFileName}`, { root: `${results.dirPath}` });
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
