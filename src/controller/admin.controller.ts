import { Request, RequestHandler, Response, query } from 'express';
import { UserService } from '../services/user.service';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { User } from '../entity/User';
import { PagedList } from '../shared/models/paged-list.model';
import { IUser } from '../shared/interfaces/IUser';
import { UserListFilter } from '../shared/models/user-list-filter.model';
import validationHelper from '../helper/validation-helper';
import { ErrorType } from '../shared/enums/error-types.enum';
import authService from '../services/auth.service';
import { EMailService } from '../services/email.service';
import { RoleService } from '../services/role.service';
import { Permission } from '../shared/enums/permissions.enum';

const userService = new UserService();
const emailService = new EMailService();
const roleService = new RoleService();

export const requestRoleUpdate: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userId = req.user?.UserId;
    if (!userId) return res.sendStatus(HttpCode.BadRequest);

    const data = await userService.setHasRequestedRoleUpdate(userId);

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getUserList: RequestHandler = async (req: Request, res: Response) => {
  try {
    let { page = '1', take = '30', search, sort, hasRequestedRoleUpdate } = req.query;

    const parsedHasRequestedRoleUpdate = validationHelper.parseBoolean(hasRequestedRoleUpdate?.toString());

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) {
      return res.status(HttpCode.BadRequest).json({ error: paramError });
    }
    
    sort = sort?.toString().toUpperCase() as 'ASC' | 'DESC' | undefined;
    const filterOptions: UserListFilter = {
      search: (search ?? '').toString(),
      sort: sort ?? 'ASC',
      hasRequestedRoleUpdate: parsedHasRequestedRoleUpdate,
    };

    const list = await userService.listFiltered(Number(take), Number(page), filterOptions);

    const data: PagedList<User[]> = {
      currentPage: Number(page),
      totalLength: list.total,
      list: list.data,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getUser: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userId = req.params.userId.toString();
    const user = await userService.readById(userId);

    res.status(HttpCode.OK).json(user);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createUser: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userData = req.body as IUser;
    const { randomPassword } = req.query;

    if (randomPassword == 'true') {
      userData.Password = authService.randomPasswordGenerator();
    }

    const checkProperties = [
      'Firstname',
      'Lastname',
      'UserId',
      'Street',
      'Postal',
      'City',
      'Housenumber',
      'Country',
      'Email',
      'Phone',
      'Password',
    ];

    if (validationHelper.checkPropertiesNullOrEmpty<IUser>(userData, checkProperties)) {
      return res.status(HttpCode.BadRequest).json({ error: ErrorType.InvalidInput });
    }

    const result = await userService.create(userData, true);

    if (result != undefined) {
      return res.status(HttpCode.BadRequest).json({ error: result });
    }

    // if (randomPassword == 'true') {
    //   // send email to user with password
    // }

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteUser: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userId = req.params.userId.toString();

    await userService.deleteById(userId);
    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateUser: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userId = req.params.userId.toString();
    const updateResource = req.body as IUser;

    await userService.patchById(userId, updateResource);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateUserSelf: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userId = req.user?.UserId;
    const updateResource = req.body as IUser;

    if (!userId) return res.sendStatus(HttpCode.Unauthorized);

    await userService.patchById(userId, updateResource, true);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateToInternalById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const userId = req.params.userId.toString();

    if (!userId) return res.sendStatus(HttpCode.BadRequest);

    const role = await roleService.getByPermission(Permission.InternalMember);

    if (!role) return res.sendStatus(HttpCode.BadRequest);

    const user = await userService.upgradeToInternalById(userId, role.RoleId);

    // const emailData = {
    //   route: {
    //     path: 'email/user-has-been-upgraded',
    //   },
    //   data: {
    //     receiver: { Email: user.Email },
    //     user: user,
    //   },
    // };

    // if (process.env.NODE_ENV === 'production') {
    //   await emailService.createEmail(emailData.route, emailData.data);
    // }

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
