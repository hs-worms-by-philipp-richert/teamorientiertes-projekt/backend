import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { OrderService } from '../services/order.service';
import { IOrder } from '../shared/interfaces/IOrder';
import { ErrorType } from '../shared/enums/error-types.enum';
import { Order } from '../entity/Order';
import { PagedList } from '../shared/models/paged-list.model';
import { OrderStatus } from '../shared/enums/order-status.enum';
import validationHelper from '../helper/validation-helper';
import { OrderFilterList } from '../shared/models/order-list-filter.model';
import { LineItemService } from '../services/line-item.service';
import { EMailService } from '../services/email.service';
import { PDFGeneratorService } from '../services/pdf.service';
import AuthService from '../services/auth.service';
import { Permission } from '../shared/enums/permissions.enum';

const orderService = new OrderService();
const emailService = new EMailService();
const pdfService = new PDFGeneratorService();
const lineItemService = new LineItemService();

export const getOrderList: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { page = '1', take = '30', search, sort: sortParam, orderDate, statusOrder } = req.query;

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    if (statusOrder && !Object.values(OrderStatus).includes(Number(statusOrder)))
      return res.sendStatus(HttpCode.BadRequest);

    const parsedPage: number = parseInt(page.toString());
    const parsedTake: number = parseInt(take.toString());
    const sort = (sortParam ?? '').toString().toUpperCase() as 'ASC' | 'DESC' | undefined;

    const filterOptions: OrderFilterList = {
      search: (search ?? '').toString(),
      sort: sort ?? 'DESC',
      orderDate: (orderDate ?? '').toString(),
      orderStatus: statusOrder ? Number(statusOrder) : undefined,
    };

    const list = await orderService.listFiltered(parsedTake, parsedPage, filterOptions);

    const data: PagedList<Order[]> = {
      currentPage: Number(page),
      list: list.data,
      totalLength: list.totalLength,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getOrderById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const orderId = BigInt(req.params.id);
    const order = await orderService.readByIdandTotal(orderId);

    if (!order) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(order);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getOrdersByUserId: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { userId } = req.params;
    const { take = '10', page = '1' } = req.query;

    if (!userId) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    if ((userId !== req.user?.UserId) && !AuthService.isAllowedTo(Permission.Admin, AuthService.parsePermissions(req.user?.Role.Permission))) {
      return res.sendStatus(HttpCode.Unauthorized);
    }

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());
    if (paramError) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedTake = parseInt(take.toString());
    const parsedPage = parseInt(page.toString());

    const order = await orderService.listByUserId(parsedTake, parsedPage, userId);

    if (!order) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(order);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createOrder: RequestHandler = async (req: Request, res: Response) => {
  try {
    const data = req.body as IOrder;
    if (!(data.LineItems?.length > 0) || data.UserId) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const userId = req.user?.UserId;

    if (!userId) return res.sendStatus(HttpCode.Unauthorized);

    data.UserId = userId;

    for (const lineItem of data.LineItems) {
      const isAvailable = await lineItemService.isLineItemAvailable(
        lineItem.ItemId,
        new Date(lineItem.StartDate),
        new Date(lineItem.EndDate),
      );
      if (!isAvailable)
        return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.LineItemUnavailable);
    }
    const orderId = await orderService.create(data);

    if (!orderId) return ErrorType.CouldNotBeCreated;

    const dataOrder: IOrder | null = await orderService.readById(orderId);

    if (!dataOrder) return ErrorType.NotFound;

    const emailDataCustomer = {
      route: {
        path: 'email/order-invoice',
      },
      orderData: {
        receiver: dataOrder.User!.Email,
        order: dataOrder,
      },
    };

    if (process.env.NODE_ENV === 'production') {
      await emailService.createEmail(emailDataCustomer.route, emailDataCustomer.orderData);

      await pdfService.create(dataOrder);
    }

    res.status(HttpCode.OK).json(orderId);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const changeOrderStatus: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { orderStatus } = req.query;
    const orderId = BigInt(req.params.id);

    if (!orderStatus) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const newOrderStatus = parseInt(orderStatus.toString());

    if (Object.values(OrderStatus).includes(newOrderStatus)) {
      (await orderService.changeOrderStatus(orderId, newOrderStatus))
        ? res.sendStatus(HttpCode.Accepted)
        : res.sendStatus(HttpCode.NotFound);
    } else {
      res.sendStatus(HttpCode.BadRequest);
    }
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const changeOrderStatusFinalizePickUp: RequestHandler = async (req: Request, res: Response) => {
  try {
    const orderIdString = req.params.id;

    if (!orderIdString || !validationHelper.validateBigInt(orderIdString)) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const orderId = BigInt(orderIdString);
    const orderStatus = OrderStatus.ReadyForPickup;

    const isStatusUpdated = await orderService.changeOrderStatus(orderId, orderStatus);

    if (!isStatusUpdated) return res.sendStatus(HttpCode.NotFound);

    const dataOrder: IOrder | null = await orderService.readById(orderId);

    if (!dataOrder) return ErrorType.NotFound;

    const emailDataCustomer = {
      route: {
        path: 'email/ready-for-pickup',
      },
      orderData: {
        receiver: dataOrder.User!.Email,
        order: dataOrder,
      },
    };

    if (process.env.NODE_ENV === 'production') {
      await emailService.createEmail(emailDataCustomer.route, emailDataCustomer.orderData);
    }

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteOrderById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const orderId = req.params.id;

    if (!orderId || !validationHelper.validateBigInt(orderId.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedOrderId = BigInt(orderId);

    const order = await orderService.readById(parsedOrderId);
    if (!order) return res.sendStatus(HttpCode.NotFound);
    if (order.OrderStatus >= 3) res.sendStatus(HttpCode.Forbidden);

    await orderService.deleteById(parsedOrderId);
    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
