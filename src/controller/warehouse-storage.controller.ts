import { RequestHandler, Request, Response } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { PagedList } from '../shared/models/paged-list.model';
import { ErrorType } from '../shared/enums/error-types.enum';
import WarehouseStorageService from '../services/warehouse-storage.service';
import { WarehouseStorage } from '../entity/WarehouseStorage';
import { IWarehouseStorage } from '../shared/interfaces/IWarehouseStorage';
import WarehouseRegionService from '../services/warehouse-region.service';
import validationHelper from '../helper/validation-helper';

const warehouseStorageService = new WarehouseStorageService();
const warehouseRegionSevice = new WarehouseRegionService();

export const getStorageList: RequestHandler = async (req: Request, res: Response) => {
  try {
    let { page = '1', take = '30' } = req.query;

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedPage: number = parseInt(page.toString());
    const parsedTake: number = parseInt(take.toString());

    const list = await warehouseStorageService.list(parsedTake, parsedPage);

    const data: PagedList<WarehouseStorage[]> = {
      currentPage: Number(page),
      list: list.data,
      totalLength: list.totalLength,
    };

    res.status(HttpCode.OK).json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getStorageById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    const foundStorage = await warehouseStorageService.readById(parsedId);

    if (!foundStorage) return res.sendStatus(HttpCode.NotFound);

    res.status(HttpCode.OK).json(foundStorage);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getStoragesByRegionId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString()))
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedId = BigInt(id);

    const foundStorages = await warehouseStorageService.readAllByRegionId(parsedId);

    if (!foundStorages) return res.sendStatus(HttpCode.NotFound);

    return res.status(HttpCode.OK).json(foundStorages);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const createStorage: RequestHandler = async (req: Request, res: Response) => {
  try {
    let data = req.body as IWarehouseStorage;

    const { Name, RegionId } = data;

    if (!Name || !RegionId)
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);

    const parsedRegionId = BigInt(RegionId);

    const foundRegion = await warehouseRegionSevice.readById(parsedRegionId);

    if (!foundRegion) return res.sendStatus(HttpCode.NotFound);

    data.RegionId = parsedRegionId;

    await warehouseStorageService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const updateStorageById: RequestHandler = async (req, res) => {
  try {
    const { id } = req.params;

    let data = req.body as IWarehouseStorage;

    if (!id || !data.RegionId || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedStorageId: bigint = BigInt(id);
    data.StorageId = parsedStorageId;
    const foundStorage = await warehouseStorageService.readById(data.StorageId);

    if (!foundStorage) return res.sendStatus(HttpCode.NotFound);

    (await warehouseStorageService.putById(parsedStorageId, data))
      ? res.sendStatus(HttpCode.Accepted)
      : res.sendStatus(HttpCode.NotFound);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const deleteStorageById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    if (!id || !validationHelper.validateBigInt(id.toString())) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedId: bigint = BigInt(id);
    const foundStorage = await warehouseStorageService.readById(parsedId);

    if (!foundStorage) return res.sendStatus(HttpCode.NotFound);

    await warehouseStorageService.deleteById(parsedId);

    res.sendStatus(HttpCode.Accepted);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
