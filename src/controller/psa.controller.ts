import { Request, RequestHandler, Response } from 'express';
import { HttpCode } from '../shared/enums/http-codes.enum';
import errorHandler from '../helper/error-handler';
import { PsaService } from '../services/psa.service';
import { ErrorType } from '../shared/enums/error-types.enum';
import { PagedList } from '../shared/models/paged-list.model';
import { IProductItem } from '../shared/interfaces/IProductItem';
import validationHelper from '../helper/validation-helper';

const psaService = new PsaService();

export const getProductItemsWithPsa: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { page = '1', take = '30', search } = req.query;

    const paramError = errorHandler.handleControllerListParameters(page.toString(), take.toString());

    if (paramError) {
      return errorHandler.handleControllerCancellation(res, HttpCode.BadRequest, ErrorType.InvalidInput);
    }

    const parsedPage: number = parseInt(page.toString());
    const parsedTake: number = parseInt(take.toString());

    const list = await psaService.listFilteredOverview(parsedTake, parsedPage, { search: search?.toString() });

    const data: PagedList<IProductItem[]> = {
      currentPage: Number(page),
      list: list.data,
      totalLength: list.total,
    };

    res.json(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getPsaListByItemId: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { itemId } = req.params;

    if (!validationHelper.validateBigInt(itemId)) {
      return res.sendStatus(HttpCode.BadRequest);
    }

    const result = await psaService.readById(BigInt(itemId));

    if (!result)
      return res.sendStatus(HttpCode.NotFound);

    res.json(result);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
