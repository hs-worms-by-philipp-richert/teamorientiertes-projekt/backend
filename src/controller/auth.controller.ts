import { RequestHandler, Request, Response, NextFunction, CookieOptions } from 'express';
import errorHandler from '../helper/error-handler';
import { HttpCode } from '../shared/enums/http-codes.enum';
import AuthService from '../services/auth.service';
import { Permission } from '../shared/enums/permissions.enum';
import { AccessToken } from '../shared/models/access-token.model';
import dotenv from 'dotenv';

dotenv.config();

export const postLogin: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { identifier, password } = req.body;

    const data = await AuthService.login(identifier, password);

    res.cookie('refreshtoken', data.refreshToken, httpCookieSettings(360));

    res.json(data);
  } catch (error) {
    if (typeof error == 'string') {
      return res.status(HttpCode.BadRequest).json({ error: error });
    }

    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const getAuthRefresh: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const accessToken = removeBearer(req.headers['authorization']);
    const refreshToken = req.cookies?.refreshtoken;

    if (!accessToken || !refreshToken) return res.sendStatus(HttpCode.Unauthorized);

    const refreshResult = await AuthService.refreshToken(accessToken, refreshToken);

    if (refreshResult?.refreshed) {
      res.setHeader('x-set-access-token', refreshResult.accessToken);
      res.cookie('refreshtoken', refreshResult.refreshToken, httpCookieSettings(360));
    }

    res.sendStatus(HttpCode.OK);
  } catch (error) {
    if (typeof error === 'string') {
      res.clearCookie('refreshtoken');
      return res.status(HttpCode.Unauthorized).json({ error });
    }

    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const authDecodeUserMiddleware: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const accessToken = removeBearer(req.headers['authorization']);

    if (!accessToken) return next();

    req.user = AuthService.decode<AccessToken>(accessToken).user;

    next();
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const authRefreshMiddleware: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const accessToken = removeBearer(req.headers['authorization']);
    const refreshToken = req.cookies?.refreshtoken;

    if (!accessToken || !refreshToken) return next();

    const refreshResult = await AuthService.refreshToken(accessToken, refreshToken);

    if (refreshResult?.refreshed) {
      res.setHeader('x-set-access-token', refreshResult.accessToken);
      res.cookie('refreshtoken', refreshResult.refreshToken, httpCookieSettings(360));
    }

    req.user = AuthService.decode<AccessToken>(refreshResult.accessToken).user;

    next();
  } catch (error) {
    if (typeof error === 'string') {
      res.clearCookie('refreshtoken');
      return res.status(HttpCode.Unauthorized).json({ error });
    }

    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const authRestrictiveMiddleware: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const accessToken = removeBearer(req.headers['authorization']);
    // const refreshToken = req.cookies?.refreshtoken;

    if (!accessToken /* || !refreshToken */) return res.sendStatus(HttpCode.Unauthorized);

    if (!AuthService.verify<AccessToken>(accessToken))
      return res.sendStatus(HttpCode.Unauthorized);

    req.user = AuthService.decode<AccessToken>(accessToken).user;

    next();
  } catch (error) {
    if (typeof error === 'string') {
      res.clearCookie('refreshtoken');
      return res.status(HttpCode.Unauthorized).json({ error });
    }

    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const testEndpoint: RequestHandler = async (req: Request, res: Response) => {
  try {
    const accessToken = removeBearer(req.headers['authorization']);

    if (!accessToken) return res.json({ msg: 'not logged in' });

    const { user } = AuthService.decode<AccessToken>(accessToken);
    const permissions = AuthService.parsePermissions(user.Role.Permission);

    res.json({
      msg: 'you are logged in',
      permissions,
      admin: AuthService.isAllowedTo(Permission.Admin, permissions),
    });
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

const authAllowClearance = async (req: Request, res: Response, next: NextFunction, permission: Permission) => {
  try {
    const accessToken = removeBearer(req.headers['authorization']);

    if (!accessToken) return res.sendStatus(HttpCode.Unauthorized);

    const { user } = AuthService.decode<AccessToken>(accessToken);
    const permissions = AuthService.parsePermissions(user.Role.Permission);

    if (!AuthService.isAllowedTo(permission, permissions)) return res.sendStatus(HttpCode.Unauthorized);

    next();
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export class AuthClearance {
  static allowAdmin(req: Request, res: Response, next: NextFunction) {
    authAllowClearance(req, res, next, Permission.Admin);
  }

  static allowWarehouseWorker(req: Request, res: Response, next: NextFunction) {
    authAllowClearance(req, res, next, Permission.WarehouseWorker);
  }

  static allowInternalMember(req: Request, res: Response, next: NextFunction) {
    authAllowClearance(req, res, next, Permission.InternalMember);
  }

  static allowMember(req: Request, res: Response, next: NextFunction) {
    authAllowClearance(req, res, next, Permission.Member);
  }
}

const httpCookieSettings = (maxAgeInMinutes: number): CookieOptions => {
  if (process.env.NODE_ENV === 'production') {
    return {
      httpOnly: true,
      path: '/',
      secure: true,
      sameSite: 'none',
      maxAge: maxAgeInMinutes*60*1000
    };
  } else {
    return {
      httpOnly: true,
      path: '/',
      domain: 'localhost',
      secure: false,
      sameSite: 'lax',
      maxAge: maxAgeInMinutes*60*1000
    };
  }
}

const removeBearer = (bearer: string | undefined): string => {
  return bearer?.replace('Bearer ', '') ?? '';
}