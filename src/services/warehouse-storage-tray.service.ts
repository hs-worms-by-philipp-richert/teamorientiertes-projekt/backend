import { AppDataSource } from '../data-source';
import { WarehouseStorageTray } from '../entity/WarehouseStorageTray';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IWarehouseStorageTray } from '../shared/interfaces/IWarehouseStorageTray';
import { CRUD } from '../shared/interfaces/crud';

class WarehouseStorageTrayService implements CRUD {
  private warehouseStorageTrayRepository;

  constructor() {
    this.warehouseStorageTrayRepository = AppDataSource.getRepository(WarehouseStorageTray);
  }

  async listFiltered(take: number, page: number, filterOptions: { id: bigint }): Promise<any> {
    const skip = (page - 1) * take;

    const [data, total] = await this.warehouseStorageTrayRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        Storage: true,
      },
      order: { Name: 'ASC' },
      where: {
        TrayId: filterOptions.id,
      },
    });

    return { data, total };
  }

  async list(take: number, page: number): Promise<any> {
    const skip: number = (page - 1) * take;

    const [data, totalLength] = await this.warehouseStorageTrayRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        Storage: true,
      },
      order: { Name: 'ASC' },
    });

    return { data, totalLength };
  }

  async create(data: IWarehouseStorageTray): Promise<boolean> {
    const { StorageId: regionId, Name: name } = data;

    let newWarehouseStorageTray = new WarehouseStorageTray();
    newWarehouseStorageTray.Name = name;
    newWarehouseStorageTray.StorageId = regionId;

    await this.warehouseStorageTrayRepository.insert(newWarehouseStorageTray);

    return true;
  }

  async putById(id: bigint, resources: IWarehouseStorageTray): Promise<boolean> {
    const result = await this.warehouseStorageTrayRepository.update({ TrayId: id }, resources);

    return result.affected !== 0;
  }

  async readById(id: any): Promise<WarehouseStorageTray | null> {
    const bigIntId = BigInt(id);

    if (!bigIntId) throw ErrorType.InvalidInput;

    const foundTray: WarehouseStorageTray | null = await this.warehouseStorageTrayRepository.findOne({
      where: {
        TrayId: bigIntId,
      },
    });

    return foundTray;
  }

  async readAllByStorageId(storageId: bigint): Promise<WarehouseStorageTray[] | null> {
    const trays: WarehouseStorageTray[] | null = await this.warehouseStorageTrayRepository.find({
      where: {
        StorageId: storageId,
      },
      order: {
        Name: 'ASC',
      },
    });

    if (trays.length <= 0) return null;

    return trays;
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.warehouseStorageTrayRepository.delete({ TrayId: id });

    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}

export default WarehouseStorageTrayService;
