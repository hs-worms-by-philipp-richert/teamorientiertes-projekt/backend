import axios from 'axios';
import { CRUD } from '../shared/interfaces/crud';
import dotenv from 'dotenv';

dotenv.config();

export class EMailService implements CRUD {
  create(resource: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }
  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  async createEmail(route: { path: string }, data: {}): Promise<void> {
    const documentServiceLink = process.env.DOCUMENTSERVICE;

    await axios.post(`${documentServiceLink}${route.path}`, data, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
