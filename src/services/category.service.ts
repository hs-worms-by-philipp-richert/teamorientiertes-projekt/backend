import { AppDataSource } from '../data-source';
import { CRUD } from '../shared/interfaces/crud';
import { Category } from '../entity/Category';
import { ICategory } from '../shared/interfaces/ICategory';
import { ProductCategoryService } from './product-category.service';
import { ProductCategory } from '../entity/ProductCategory';
import { IsNull, Not } from 'typeorm';
import { PagedList, PagedListCategory } from '../shared/models/paged-list.model';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { Product } from '../entity/Product';


export class CategoryService implements CRUD {
  private categoryRepository;
  private productCategoryService: ProductCategoryService;

  constructor() {
    this.categoryRepository = AppDataSource.getRepository(Category);
    this.productCategoryService = new ProductCategoryService();
  }

  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async listFiltered(take: number, page: number, options: { showEmptyCategory: boolean, cascadeSubcategory: boolean }) {
    const skip = (page - 1) * take;
  
    const cascadeChildCategoriesWhere = {
      ParentCategoryId: IsNull()
    };

    if (options.showEmptyCategory) {
      const [data, total] = await this.categoryRepository.findAndCount({
        skip: take == 0 ? undefined : skip,
        take: take == 0 ? undefined : take,
        order: { Name: 'ASC' },
        relations: {
          ParentCategoryId: true,
          ChildCategories: options.cascadeSubcategory
        },
        select: {
          Name: true,
          CategoryId: true,
          ParentCategoryId: {
            CategoryId: true
          },
          ChildCategories: options.cascadeSubcategory
        },
        where: options.cascadeSubcategory ? cascadeChildCategoriesWhere : {}
      });
      return { data, total };
    }

    const [data, total] = await this.categoryRepository.findAndCount({
      skip: skip,
      take: take,
      order: { Name: 'ASC' },
      relations: {
        ParentCategoryId: true,
        ChildCategories: options.cascadeSubcategory
      },
      select: {
        Name: true,
        CategoryId: true,
        ParentCategoryId: {
          CategoryId: true
        },
        ChildCategories: options.cascadeSubcategory
      },
      where: options.cascadeSubcategory ? [
        {
          ...cascadeChildCategoriesWhere,
          ChildCategories: { ProductCategories: { ProductId: Not(IsNull()) } }
        },
        {
          ...cascadeChildCategoriesWhere,
          // ChildCategories: { ProductCategories: IsNull() },    // this should hide empty children-categories, but is currently not supported by typeorm
          ProductCategories: { ProductId: Not(IsNull()) }
        }
      ] : {
        ProductCategories: { ProductId: Not(IsNull()) }
      }

    });

    return { data, total };
  }

  async create(resource: ICategory): Promise<void> {
    await this.categoryRepository.save(resource);
  }

  async putById(id: bigint, resource: ICategory): Promise<boolean> {
    const result = await this.categoryRepository.update({ CategoryId: id }, resource);

    return result.affected !== 0;
  }

  async readById(id: bigint): Promise<Category | null> {
    return await this.categoryRepository.findOne({
      where: {
        CategoryId: id
      },
      relations: {
        ParentCategoryId: true
      },
      select: {
        Name: true,
        CategoryId: true,
        ParentCategoryId: {
          CategoryId: true
        }
      }
    });
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.categoryRepository.manager.transaction(async (transactionalEntityManager) => {
      const data = {
        productId: BigInt(0),
        categoryId: BigInt(id),
        transactionalEntityManager: transactionalEntityManager,
      };
      await this.productCategoryService.deleteAllById(data);
      await transactionalEntityManager.delete(Category, { CategoryId: id });
    });

    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async getCategoryProductsById(
    id: bigint,
    take: number,
    page: number
  ): Promise<PagedListCategory<Product[], Category>> {
    const category: Category | null = await this.readById(id);

    if (!category)
      throw HttpCode.NotFound;

    let subCatgories: Category[] = [];

    if (category != null) {
      subCatgories = (await this.categoryRepository.find({ where: { ParentCategoryId: category } })) || [];
    }

    const productCategoryList: PagedList<ProductCategory[]> = await this.productCategoryService.listFiltered(take, page, { CategoryId: id });
    
    const allProducts: Product[] = productCategoryList.list.map(m => m.Product);

    return {
      category,
      subCatgories,
      pagedList: {
        currentPage: productCategoryList.currentPage,
        totalLength: productCategoryList.totalLength,
        list: allProducts
      }
    };
  }
}

