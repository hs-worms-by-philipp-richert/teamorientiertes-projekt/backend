import { AppDataSource } from '../data-source';
import { CRUD } from '../shared/interfaces/crud';
import { IProductVariantProperty } from '../shared/interfaces/IProductVariantProperty';
import { ProductVariantProperty } from '../entity/ProductVariantProperty';
import { EntityManager } from 'typeorm';

export class ProductVariantPropertyService implements CRUD {
  private productVariantPropertyRepository;

  constructor() {
    this.productVariantPropertyRepository = AppDataSource.getRepository(
      ProductVariantProperty
    );
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.productVariantPropertyRepository.delete({PropertyId: id});
    return true;
  }
  async putById(id: any, name: string): Promise<boolean> {
    await this.productVariantPropertyRepository.update(
      { PropertyId: id },
      { Name: name }
    );
    return true;
  }

  async readById(id: bigint): Promise<IProductVariantProperty | null> {
    return await this.productVariantPropertyRepository.findOne({
      where: {
        PropertyId: id,
      },
    });
  }

  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  
  async list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async getAllByVariantId(variantId: bigint) {
    return await this.productVariantPropertyRepository.find({
      where: {
        VariantId: variantId,
      },
      order: { VariantId: 'ASC' },
    });
  }

  async create(resource: IProductVariantProperty): Promise<void> {
    await this.productVariantPropertyRepository.save(resource);
  }

  async deleteAllByVariantId(
    variantId: bigint,
    transactionalEntityManager: EntityManager
  ): Promise<void> {
    await transactionalEntityManager.delete(ProductVariantProperty, {
      VariantId: variantId,
    });
  }

  async patchById(id: bigint, name: string): Promise<boolean> {
    await this.productVariantPropertyRepository.update(
      { PropertyId: id },
      { Name: name }
    );
    return true;
  }
}
