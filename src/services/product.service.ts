import { AppDataSource } from '../data-source';
import { Product } from '../entity/Product';
import { CRUD } from '../shared/interfaces/crud';
import findOptionsHelper from '../helper/find-options.helper';
import { ProductListFilter } from '../shared/models/product-list-filter.model';
import { FilterOption } from '../helper/filter-options.helper';
import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsSelect,
  FindOptionsWhere,
  In,
  IsNull,
  LessThan,
  LessThanOrEqual,
  MoreThan,
  MoreThanOrEqual,
  Not,
  Repository,
} from 'typeorm';
import { IProduct } from '../shared/interfaces/IProduct';
import { ProductCategoryService } from './product-category.service';
import { ProductItemService } from './product-item.service';
import { ProductVariantService } from './product-variant.service';
import { ProductItem } from '../entity/ProductItem';
import FileService from './file.service';

export class ProductService implements CRUD {
  private productRepository: Repository<Product>;
  private productCategoryService: ProductCategoryService;
  private productItemService: ProductItemService;
  private productVariantService: ProductVariantService;
  private fileService: FileService;

  constructor() {
    this.productRepository = AppDataSource.getRepository(Product);
    this.productCategoryService = new ProductCategoryService();
    this.productItemService = new ProductItemService();
    this.productVariantService = new ProductVariantService();
    this.fileService = new FileService();
  }
  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async listFiltered(take: number, page: number, filterOptions: any) {
    const filterOption = new FilterOption();
    const skip = (page - 1) * take;
    const filterOptionsTyped: ProductListFilter = filterOptions as ProductListFilter;

    const orderParams: { key: string; value: any } = {
      key: filterOptionsTyped.sortBy,
      value: filterOptionsTyped.sort,
    };

    const allFilterOptions = filterOption.filter(filterOptionsTyped);

    const whereCascade = findOptionsHelper.buildWhere<Product>(
      allFilterOptions.filterOrArray,
      allFilterOptions.filterAndArray,
      allFilterOptions.filterRelations[0],
    );

    if (filterOptionsTyped.startDate != '' && filterOptionsTyped.endDate != '') {
      whereCascade.forEach((m) => {
        m.ProductItems = [
          {
            LineItems: {
              EndDate: LessThan(new Date(filterOptionsTyped.startDate)),
            },
          },
          {
            LineItems: {
              StartDate: MoreThan(new Date(filterOptionsTyped.endDate)),
            },
          },
          {
            ProductId: Not(IsNull()),
            LineItems: {
              LineItemId: IsNull(),
            },
          },
        ];
      });
    }

    let { data: dataAvailable, total: totalAvailable } = await this.getProductsGenericWhere(
      skip,
      take,
      whereCascade,
      findOptionsHelper.createOrderOptions(orderParams),
    );

    const allAvailableProducts = await this.productRepository.find({
      where: whereCascade,
      select: {
        ProductId: true,
      },
    });
    const allProductIds = allAvailableProducts.map((m) => m.ProductId);

    let dataUnavailable: Product[] = [];
    let totalUnavailable: number = 0;

    if (dataAvailable.length < take && filterOptionsTyped.startDate != '' && filterOptionsTyped.endDate != '') {
      whereCascade.forEach((m) => {
        m.ProductItems = [
          {
            ProductId: Not(In(allProductIds)),
          },
          {
            ProductId: IsNull(),
          },
        ];
      });

      const skipUnavailable = skip - totalAvailable;
      const { data: tmpData, total: tmpTotal } = await this.getProductsGenericWhere(
        skipUnavailable < 0 ? 0 : skipUnavailable,
        take - dataAvailable.length,
        whereCascade,
        findOptionsHelper.createOrderOptions(orderParams),
      );

      dataUnavailable = tmpData;
      totalUnavailable = tmpTotal;
    }

    // commented because we decided to display it in frontend
    // if (filterOptionsTyped.isInternal)
    //   dataAvailable = dataAvailable.map((m) => {
    //     return { ...m, RentingFee: 0 };
    //   });

    // if (filterOptionsTyped.isInternal)
    //   dataUnavailable = dataUnavailable.map((m) => {
    //     return { ...m, RentingFee: 0 };
    //   });

    return {
      dataAvailable,
      dataUnavailable,
      totalAvailable,
      totalUnavailable,
      page,
    };
  }

  async listAvailableItemsByProductId(productId: bigint, startDate: Date, endDate: Date) {
    const relations: FindOptionsRelations<Product> = {
      ProductItems: {
        Variant: {
          Properties: true,
        },
      },
    };

    const select: FindOptionsSelect<Product> = {
      ProductId: true,
      ProductItems: {
        ItemId: true,
        QrCode: true,
        Serialnumber: true,
        ProductId: true,
        VariantId: true,
      },
    };

    const whereRangeAvailable: FindOptionsWhere<ProductItem>[] = [
      {
        LineItems: {
          EndDate: LessThan(startDate),
        },
      },
      {
        LineItems: {
          StartDate: MoreThan(endDate),
        },
      },
      {
        LineItems: {
          LineItemId: IsNull(),
        },
      },
    ];

    const whereRangeUnavailable: FindOptionsWhere<ProductItem>[] = [
      {
        LineItems: {
          StartDate: LessThanOrEqual(endDate),
          EndDate: MoreThanOrEqual(startDate),
        },
      },
    ];

    const product = await this.productRepository.findOne({
      where: { ProductId: productId },
      select: { ProductId: true, Name: true },
    });

    const availableItems = await this.productRepository.find({
      relations,
      select,
      where: {
        ProductId: productId,
        ProductItems: whereRangeAvailable,
      },
    });

    const unavailableItems = await this.productRepository.find({
      relations,
      select,
      where: {
        ProductId: productId,
        ProductItems: whereRangeUnavailable,
      },
    });

    const unavailableItemsFormatted = unavailableItems[0]?.ProductItems;

    const availableItemsFormatted = availableItems[0]?.ProductItems?.filter((item) => {
      if (unavailableItemsFormatted?.find((m) => m.ItemId == item.ItemId)) return false;

      return true;
    });

    return {
      product,
      availableItems: availableItemsFormatted ?? [],
      unavailableItems: unavailableItemsFormatted ?? [],
    };
  }

  async create(resource: IProduct): Promise<bigint> {
    const result = await this.productRepository.save(resource);
    return result.ProductId;
  }

  async putById(id: bigint, resource: IProduct): Promise<boolean> {
    const result = await this.productRepository.update({ ProductId: id }, resource);

    return result.affected !== 0;
  }

  async readById(id: any): Promise<Product | null> {
    return await this.productRepository.findOne({
      where: { ProductId: id },
      relations: {
        ProductItems: {
          Variant: {
            Properties: true,
          },
          Tray: true,
        },
        Categories: {
          Category: true,
        },
        ProductImages: true,
        ProductManuals: true,
        Variants: true,
      },
    });
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.productRepository.manager.transaction(async (transactionalEntityManager) => {
      let foundItems = await transactionalEntityManager.find(ProductItem, { where: { ProductId: id } });
      for (const item of foundItems) {
        await this.productItemService.deleteByIdTransaction(item.ItemId, transactionalEntityManager);
      }
      foundItems = await transactionalEntityManager.find(ProductItem, { where: { ProductId: id } });

      if (foundItems.length > 0) return true;
      const data = {
        productId: BigInt(id),
        categoryId: BigInt(0),
        transactionalEntityManager: transactionalEntityManager,
      };
      await this.productCategoryService.deleteAllById(data);
      await this.productVariantService.deleteByProductId(data);

      const foundImage = await this.fileService.getFilesByProductId(data.productId, 'product-image');
      if (foundImage && foundImage.length > 0) {
        await this.fileService.deleteDirectoryByIdentifier(id, 'product-image');
      }
      const foundManual = await this.fileService.getFilesByProductId(data.productId, 'product-manual');
      if (foundManual && foundManual.length > 0) {
        await this.fileService.deleteDirectoryByIdentifier(id, 'product-manual');
      }
      await transactionalEntityManager.delete(Product, { ProductId: id });
    });
    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  private async getProductsGenericWhere(
    skip: number,
    take: number,
    where: FindOptionsWhere<Product> | FindOptionsWhere<Product>[],
    order: FindOptionsOrder<Product>,
  ) {
    const repository = await this.productRepository.findAndCount({
      skip,
      take,
      relations: {
        ProductItems: {
          Variant: {
            Properties: true,
          },
          Tray: true,
          LineItems: true,
        },
        Categories: {
          Category: true,
        },
        ProductImages: true,
        ProductManuals: true,
        Variants: true,
      },
      order,
      where,
    });

    const [data, total] = repository;

    return { data, total };
  }
}
