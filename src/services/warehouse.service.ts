import { AppDataSource } from '../data-source';
import { Warehouse } from '../entity/Warehouse';
import { IWarehouse } from '../shared/interfaces/IWarehouse';
import { CRUD } from '../shared/interfaces/crud';

class WarehouseService implements CRUD {
  private warehouseRepository;
  constructor() {
    this.warehouseRepository = AppDataSource.getRepository(Warehouse);
  }
  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async list(take: number, page: number): Promise<{ data: Warehouse[]; totalLength: number }> {
    const skip: number = (page - 1) * take;

    const [data, totalLength] = await this.warehouseRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        Regions: true,
      },
      order: { Name: 'ASC' },
    });

    return { data, totalLength };
  }

  async create(data: IWarehouse): Promise<any> {
    return await this.warehouseRepository.insert(data);
  }

  async putById(data: IWarehouse): Promise<boolean> {
    const result = await this.warehouseRepository.update(
      { WarehouseId: data.WarehouseId },
      {
        City: data.City,
        Country: data.Country,
        Housenumber: data.Housenumber,
        Name: data.Name,
        Postal: data.Postal,
        Street: data.Street,
      },
    );

    return result.affected !== 0;
  }

  async readById(id: bigint): Promise<Warehouse | null> {
    const foundWarehouse = await this.warehouseRepository.findOne({
      where: {
        WarehouseId: id,
      },
    });

    return foundWarehouse;
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.warehouseRepository.delete({ WarehouseId: id });

    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}

export default WarehouseService;
