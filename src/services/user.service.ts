import { Repository } from 'typeorm';
import { AppDataSource } from '../data-source';
import { User } from '../entity/User';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IUser } from '../shared/interfaces/IUser';
import { CRUD } from '../shared/interfaces/crud';
import AuthService from './auth.service';
import { FilterOption } from '../helper/filter-options.helper';
import findOptionsHelper from '../helper/find-options.helper';
import { UserListFilter } from '../shared/models/user-list-filter.model';
import { Role } from '../entity/Role';
import { IRole } from '../shared/interfaces/IRole';
import authService from './auth.service';

export class UserService implements CRUD {
  private userRepository: Repository<User>;
  private roleRepository: Repository<Role>;

  constructor() {
    this.userRepository = AppDataSource.getRepository(User);
    this.roleRepository = AppDataSource.getRepository(Role);
  }

  async create(resource: IUser, createdByAdmin?: boolean): Promise<ErrorType | undefined> {
    const result = await AuthService.register(resource, createdByAdmin ?? false);

    return result;
  }

  async listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    const filterOption = new FilterOption();
    const filterOptionsTyped: UserListFilter = filterOptions as UserListFilter;
    const allFilterOptions = filterOption.userFilter(filterOptionsTyped);
    const orderParams: { key: string; value: any } = {
      key: 'Firstname',
      value: filterOptionsTyped.sort,
    };
    const whereCascade = findOptionsHelper.buildWhere<User>(
      allFilterOptions.filterOrArray,
      allFilterOptions.filterAndArray,
      allFilterOptions.filterRelations[0],
    );

    const skip = (page - 1) * take;
    const [data, total] = await this.userRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        Role: true,
      },
      order: findOptionsHelper.createOrderOptions(orderParams),
      where: whereCascade,
    });

    return { data, total };
  }

  async list(take: number, page: number): Promise<any> {
    const skip = (page - 1) * take;
    const [data, total] = await this.userRepository.findAndCount({
      skip: skip,
      take: take,
      select: {
        Firstname: true,
        Lastname: true,
        UserId: true,
        Street: true,
        Postal: true,
        City: true,
        Housenumber: true,
        Country: true,
        Email: true,
        Phone: true,
        IsVerified: true,
      },
      relations: {
        Orders: true,
        Role: true,
      },
      order: { Firstname: 'ASC' },
    });

    return { data, total };
  }

  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async readById(id: string): Promise<User | null> {
    return await this.userRepository.findOne({
      where: {
        UserId: id,
      },
      select: {
        Firstname: true,
        Lastname: true,
        UserId: true,
        Street: true,
        Postal: true,
        City: true,
        Housenumber: true,
        Country: true,
        Email: true,
        Phone: true,
        IsVerified: true,
      },
      relations: {
        Orders: true,
        Role: true,
      },
    });
  }

  // async setMaterialOrderAcknowledgement(userId: string): Promise<boolean> {
  //   // const result = await this.userRepository.update({ UserId: userId }, { HasAcknowledgedMaterialOrder: true });

  //   return result.affected !== 0;
  // }

  async deleteById(id: string): Promise<boolean> {
    const response = await this.userRepository.delete({ UserId: id });
    return response.affected !== 0;
  }

  async upgradeToInternalById(id: string, roleId: number): Promise<User> {
    const user = await this.readById(id);
    if (!user) throw ErrorType.IDNotFound;

    await this.userRepository.update(user.UserId, { RoleId: roleId, HasRequestedRoleUpdate: false });

    return user;
  }

  async setHasRequestedRoleUpdate(id: string): Promise<User> {
    const user = await this.readById(id);
    if (!user) throw ErrorType.IDNotFound;

    await this.userRepository.update(user.UserId, { HasRequestedRoleUpdate: true });

    return user;
  }

  async patchById(id: any, resources: IUser, self = false): Promise<boolean> {
    const user = await this.readById(id.replace(/\//g, '-'));
    if (!user) {
      throw ErrorType.IDNotFound;
    }

    resources.UserId = user.UserId;

    if (self) {
      delete resources.HasRequestedRoleUpdate;
      delete resources.Orders;
      resources.IsVerified = user.IsVerified;
      resources.RoleId = user.RoleId;
    } else {
      if (resources.RoleId) {
        const userRoleId = resources.RoleId;
        const role: Role | null = await this.roleRepository.findOne({ where: { RoleId: userRoleId } });
        if (!role) {
          throw ErrorType.NotUpdated;
        }
        const userRole: IRole = { RoleId: role.RoleId, Name: role.Name, Permission: role.Permission };
        resources.Role = userRole;
      }
    }

    if (resources.Password) {
      resources.Password = await authService.hashPassword(resources.Password);
    }

    await this.userRepository.update(id, resources);

    return true;
  }
}
