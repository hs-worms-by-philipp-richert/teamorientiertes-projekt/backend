import { isDate } from 'util/types';
import { AppDataSource } from '../data-source';
import { LineItem } from '../entity/LineItem';
import { Order } from '../entity/Order';
import { ErrorType } from '../shared/enums/error-types.enum';
import { OrderStatus } from '../shared/enums/order-status.enum';
import { IOrder } from '../shared/interfaces/IOrder';
import { CRUD } from '../shared/interfaces/crud';
import { ProductItem } from '../entity/ProductItem';
import { FilterOption } from '../helper/filter-options.helper';
import { OrderFilterList } from '../shared/models/order-list-filter.model';
import findOptionsHelper from '../helper/find-options.helper';
import { ShowOrder } from '../shared/models/show-order.model';
import authService from './auth.service';
import { Permission } from '../shared/enums/permissions.enum';
import { ILineItem } from '../shared/interfaces/ILineItem';
import { IProductItem } from '../shared/interfaces/IProductItem';
import { IProduct } from '../shared/interfaces/IProduct';
import { MoreThanOrEqual } from 'typeorm';

export class OrderService implements CRUD {
  private orderRepository;

  constructor() {
    this.orderRepository = AppDataSource.getRepository(Order);
  }
  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    const skip: number = (page - 1) * take;
    const filterOption = new FilterOption();
    const filterOptionsTyped: OrderFilterList = filterOptions as OrderFilterList;
    const allFilterOptions = filterOption.orderFilter(filterOptionsTyped);
    const orderParams: { key: string; value: any } = {
      key: 'OrderDate',
      value: filterOptionsTyped.sort,
    };

    const whereCascade = findOptionsHelper.buildWhere<Order>(
      allFilterOptions.filterOrArray,
      allFilterOptions.filterAndArray,
      allFilterOptions.filterRelations[0],
    );

    const [data, totalLength] = await this.orderRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        LineItems: {
          Item: true,
        },
        User: {
          Role: true,
        },
      },
      order: findOptionsHelper.createOrderOptions(orderParams),
      where: whereCascade,
    });

    return { data, totalLength };
  }
  async list(take: number, page: number): Promise<any> {
    const skip: number = (page - 1) * take;

    const [data, totalLength] = await this.orderRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        LineItems: {
          Item: {
            Product: true,
          },
        },
        User: true,
      },
      order: { OrderDate: 'DESC' },
    });

    return { data, totalLength };
  }

  async listByUserId(take: number, page: number, userId: string): Promise<any> {
    const skip: number = (page - 1) * take;

    const [data, totalLength] = await this.orderRepository.findAndCount({
      skip: skip,
      take: take,
      where: {
        UserId: userId,
      },
      relations: {
        LineItems: {
          Item: {
            Product: true,
          },
        },
      },
      order: { OrderDate: 'DESC' },
    });

    return { data, totalLength };
  }

  async create(order: IOrder): Promise<bigint | null> {
    order.OrderId = undefined;
    order.OrderDate = new Date();
    order.OrderStatus = OrderStatus.Open;

    let newOrderId: bigint | null = null;

    await this.orderRepository.manager.transaction(async (transactionalEntityManager) => {
      const result = await transactionalEntityManager.insert(Order, order);

      for await (const lineItem of order.LineItems) {
        lineItem.LineItemId = undefined;
        lineItem.StartDate = new Date(lineItem.StartDate);
        lineItem.EndDate = new Date(lineItem.EndDate);
        lineItem.ReturnDate = undefined;

        if (
          !lineItem.ItemId ||
          !BigInt(lineItem.ItemId) ||
          !isDate(lineItem.StartDate) ||
          !isDate(lineItem.EndDate) ||
          lineItem.StartDate > lineItem.EndDate
        )
          throw ErrorType.InvalidInput;

        const foundItem = await transactionalEntityManager.findOne(ProductItem, {
          where: {
            ItemId: lineItem.ItemId,
          },
        });
        if (!foundItem) throw ErrorType.InvalidInput;

        lineItem.OrderId = BigInt(result.identifiers[0].OrderId);
        transactionalEntityManager.insert(LineItem, lineItem);
      }

      newOrderId = result.identifiers[0].OrderId;
    });

    return newOrderId;
  }

  async readById(id: bigint): Promise<IOrder | null> {
    return this.orderRepository.findOne({
      where: {
        OrderId: id,
      },
      relations: {
        LineItems: {
          Item: {
            Product: {
              ProductImages: true,
            },
          },
        },
        User: {
          Role: true,
        },
      },
    });
  }
  async readByIdandTotal(id: bigint): Promise<ShowOrder<IOrder> | null> {
    let order: IOrder | null = await this.readById(id);
    let isInternal: boolean = false;

    let totalSum: number = 0;
    if (order) {
      isInternal = authService.isAllowedTo(
        Permission.InternalMember,
        authService.parsePermissions(order.User!.Role.Permission),
      );
      if (isInternal) {
        order.LineItems = order.LineItems.map((item: ILineItem) => {
          const productItem: IProductItem = item.Item!;
          const product: IProduct = productItem.Product!;
          return { ...item, Item: { ...productItem, Product: { ...product, RentingFee: 0 } } };
        });
      } else {
        order.LineItems.map((item: ILineItem) => {
          const productItem: IProductItem = item.Item!;
          const product: IProduct = productItem.Product!;
          totalSum += Number(product.RentingFee);
        });
      }
    }

    const finalValue: ShowOrder<IOrder> = {
      Order: order!,
      total: totalSum,
    };

    return finalValue;
  }
  async deleteById(id: bigint): Promise<boolean> {
    await this.orderRepository.manager.transaction(async (transactionalEntityManager) => {
      await transactionalEntityManager.delete(LineItem, { OrderId: id });
      await transactionalEntityManager.delete(Order, { OrderId: id });
    });

    return true;
  }
  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  async changeOrderStatus(id: bigint, orderStatus: OrderStatus): Promise<boolean> {
    const result = await this.orderRepository.update({ OrderId: id }, { OrderStatus: orderStatus });

    return result.affected !== 0;
  }

  async todaysOrders() {
    const today = new Date();
    const dayAgo = new Date(new Date().setDate(today.getDate() - 1));

    const results = this.orderRepository.find({
      where: {
        OrderDate: MoreThanOrEqual(dayAgo),
      },
      relations: {
        User: true,
        LineItems: true,
      },
    });

    return results;
  }

  async getAllExceeded() {
    const today = new Date();

    const qb = this.orderRepository
      .createQueryBuilder('order')
      .innerJoinAndSelect('order.LineItems', 'lineItem')
      .innerJoinAndSelect('lineItem.Item', 'productItem')
      .innerJoinAndSelect('productItem.Product', 'product')
      .innerJoinAndSelect('order.User', 'user')
      .where('order.OrderStatus < :archivedStatus', { archivedStatus: OrderStatus.Archived })
      .andWhere('lineItem.EndDate < :today', { today: today });

    const result = await qb.getMany();
    return result;
  }

  async getAllTwoDaysRemaining() {
    const today = new Date();
    const twoDaysAhead = new Date();
    twoDaysAhead.setDate(today.getDate() + 2);

    const qb = this.orderRepository
      .createQueryBuilder('order')
      .innerJoinAndSelect('order.LineItems', 'lineItem')
      .innerJoinAndSelect('lineItem.Item', 'productItem')
      .innerJoinAndSelect('productItem.Product', 'product')
      .innerJoinAndSelect('order.User', 'user')
      .where('order.OrderStatus < :archivedStatus', { archivedStatus: OrderStatus.Archived })
      .andWhere('lineItem.StartDate < :today', { today: today })
      .andWhere('lineItem.EndDate = :twoDaysAhead', { twoDaysAhead: twoDaysAhead });

    const result = qb.getMany();
    return result;
  }

  async getAllOneDayRemaining() {
    const today = new Date();
    const oneDayAhead = new Date();
    oneDayAhead.setDate(today.getDate() + 1);

    const qb = this.orderRepository
      .createQueryBuilder('order')
      .innerJoinAndSelect('order.LineItems', 'lineItem')
      .innerJoinAndSelect('lineItem.Item', 'productItem')
      .innerJoinAndSelect('productItem.Product', 'product')
      .innerJoinAndSelect('order.User', 'user')
      .where('order.OrderStatus < :archivedStatus', { archivedStatus: OrderStatus.Archived })
      .andWhere('lineItem.StartDate < :today', { today: today })
      .andWhere('lineItem.EndDate = :oneDayAhead', { oneDayAhead: oneDayAhead });

    const result = qb.getMany();
    return result;
  }

  async getAllNoDayRemaining() {
    const today = new Date();

    const qb = this.orderRepository
      .createQueryBuilder('order')
      .innerJoinAndSelect('order.LineItems', 'lineItem')
      .innerJoinAndSelect('lineItem.Item', 'productItem')
      .innerJoinAndSelect('productItem.Product', 'product')
      .innerJoinAndSelect('order.User', 'user')
      .where('order.OrderStatus < :archivedStatus', { archivedStatus: OrderStatus.Archived })
      .andWhere('lineItem.StartDate < :today', { today: today })
      .andWhere('lineItem.EndDate = :today', { today: today });

    const result = qb.getMany();
    return result;
  }
}
