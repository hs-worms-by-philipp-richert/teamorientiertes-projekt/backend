import { And, LessThan, LessThanOrEqual, MoreThanOrEqual, Or } from 'typeorm';
import { AppDataSource } from '../data-source';
import { LineItem } from '../entity/LineItem';
import { Order } from '../entity/Order';
import { OrderStatus } from '../shared/enums/order-status.enum';
import { ILineItem } from '../shared/interfaces/ILineItem';
import { CRUD } from '../shared/interfaces/crud';

export class LineItemService implements CRUD {
  private lineItemRepository;
  private orderRepository;

  constructor() {
    this.lineItemRepository = AppDataSource.getRepository(LineItem);
    this.orderRepository = AppDataSource.getRepository(Order);
  }

  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async create(lineItem: ILineItem): Promise<any> {
    return this.lineItemRepository.insert(lineItem);
  }

  async putById(id: bigint, endDate: Date): Promise<boolean> {
    const result = await this.lineItemRepository.update({ LineItemId: id }, { EndDate: endDate });

    return result.affected !== 0;
  }

  async setReturnDateById(id: bigint, date?: Date | null): Promise<boolean> {
    const result = await this.lineItemRepository.update({ LineItemId: id }, { ReturnDate: date });

    return result.affected !== 0;
  }

  async readByProductItemId(id: bigint): Promise<any> {
    return await this.lineItemRepository.find({
      where: { ItemId: id },
    });
  }

  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async deleteById(id: bigint): Promise<boolean> {
    const foundItem = await this.lineItemRepository.findOne({
      where: {
        ItemId: id,
      },
    });
    if (!foundItem) return false;

    const orderId: bigint = foundItem.OrderId;

    const foundOrder = await this.orderRepository.findOne({
      where: {
        OrderId: orderId,
      },
    });

    if (!foundOrder) return false;

    if (foundOrder.OrderStatus >= 3) return false;

    await this.lineItemRepository.delete({ LineItemId: id });

    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async isLineItemAvailable(itemId: bigint, startDate: Date, endDate: Date): Promise<boolean> {
    if (Number(new Date(startDate.toDateString())) < Number(new Date(new Date().toDateString()))) return false;

    const unavailableItems = await this.lineItemRepository.find({
      where: {
        ItemId: itemId,
        StartDate: LessThanOrEqual(endDate),
        EndDate: MoreThanOrEqual(startDate),
      },
    });

    return unavailableItems.length === 0;
  }
}
