import { CRUD } from '../shared/interfaces/crud';
import fs from 'fs/promises';
import { convertToBase64 } from '../helper/convert-to-base64';
import axios from 'axios';
import { OrderService } from './order.service';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IOrder } from '../shared/interfaces/IOrder';

export class PDFGeneratorService implements CRUD {
  private orderService;

  constructor() {
    this.orderService = new OrderService();
  }

  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }
  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  async create(orderData: IOrder): Promise<any> {
    const { OrderDate: orderDate, OrderId: orderName, UserId: userId } = orderData;
    const dirPath = `./uploads/order-pdf/user-${userId}`;
    const orderFileName = `/order-${orderName}-${orderDate}.pdf`;
    const logoPath = 'public/assets/img/user-content/main-logo.svg';

    try {
      await fs.access(dirPath);
    } catch (error) {
      await fs.mkdir(dirPath, { recursive: true });
    }
    const logoDataUri = await convertToBase64(logoPath);

    if (!logoDataUri) throw ErrorType.FileConversionError;

    const documentServiceLink = process.env.DOCUMENTSERVICE;

    const pdfGeneratorLink = `${documentServiceLink}pdf/generate/order-invoice`;

    const response = await axios.post(pdfGeneratorLink, orderData, {
      responseType: 'arraybuffer',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    await fs.writeFile(dirPath + orderFileName, response.data);
    return true;
  }

  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async readPDF(orderId: bigint) {
    const orderData = await this.orderService.readById(orderId);

    if (!orderData) throw ErrorType.NotFound;

    const dirPath = `./uploads/order-pdf/user-${orderData.UserId}`;
    const orderFileName = `/order-${orderData.OrderId}-${orderData.OrderDate}.pdf`;
    const path = dirPath + orderFileName;

    await fs.stat(path);

    return { dirPath: dirPath, orderFileName: orderFileName};
  }

  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
