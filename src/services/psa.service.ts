import { Between, ILike, LessThan } from 'typeorm';
import { AppDataSource } from '../data-source';
import { ProductItem } from '../entity/ProductItem';
import { PsaCheck } from '../entity/PsaCheck';
import { CRUD } from '../shared/interfaces/crud';

export class PsaService implements CRUD {
  private psaCheckRepository;
  private productItemRepository;

  constructor() {
    this.psaCheckRepository = AppDataSource.getRepository(PsaCheck);
    this.productItemRepository = AppDataSource.getRepository(ProductItem);
  }

  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async listFiltered(take: number, page: number, filterOptions: any) {
    throw new Error('Method not implemented.');
  }

  async listFilteredOverview(take: number, page: number, filterOptions: { search: string | undefined }) {
    const skip: number = (page - 1) * take;

    const [data, total] = await this.productItemRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        PSA: true,
        Product: true,
        Variant: {
          Properties: true,
        },
      },
      select: {
        ItemId: true,
        QrCode: true,
        Serialnumber: true,
        ProductId: true,
        VariantId: true,
        TrayId: true,
        ExpiresInYears: true,
        ManufacturingDate: true,
      },
      order: { PSA: { DateChecked: 'DESC' } },
      where: {
        IsDefect: false,
        Archived: false,
        // ExpiresInYears: Not(IsNull()),
        // ManufacturingDate: Not(IsNull()),
        IsPsa: true,
        ...(filterOptions.search && {
          Product: {
            Name: ILike(`%${filterOptions.search}%`),
          },
        }),
      },
    });

    return { data: data.sort(this.sortPsa), total };
  }

  private sortPsa(a: ProductItem, b: ProductItem) {
    if (a.PSA.length === 0) return 1;
    else if (b.PSA.length === 0) return -1;

    return Number(a.PSA[0].DateChecked) - Number(b.PSA[0].DateChecked);
  }

  create(resource: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async readById(id: bigint) {
    const result = await this.productItemRepository.findOne({
      relations: {
        PSA: true,
        Product: true,
        Variant: true,
        Tray: {
          Storage: {
            Region: {
              Warehouse: true,
            },
          },
        },
      },
      select: {
        ItemId: true,
        QrCode: true,
        Serialnumber: true,
        ProductId: true,
        VariantId: true,
        TrayId: true,
      },
      order: { PSA: { DateChecked: 'DESC' } },
      where: {
        ItemId: id,
      },
    });

    return result;
  }

  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async nextMonthExpiringPSAs() {
    const today = new Date();
    const elevenMonthsAgo = new Date(today);
    const yearAgo = new Date(today);

    elevenMonthsAgo.setMonth(today.getMonth() - 11);
    yearAgo.setFullYear(today.getFullYear() - 1);

    const results = await this.psaCheckRepository.findAndCount({
      where: {
        DateChecked: Between(yearAgo, elevenMonthsAgo),
      },
      relations: {
        Item: {
          Product: true,
          Tray: {
            Storage: {
              Region: {
                Warehouse: true,
              },
            },
          },
        },
      },
    });

    return results;
  }

  async expiredPSAs() {
    const today = new Date();
    const lastCheckedYearAgo = new Date(today);

    lastCheckedYearAgo.setMonth(today.getMonth() - 12);

    const results = await this.psaCheckRepository.findAndCount({
      where: {
        DateChecked: LessThan(lastCheckedYearAgo),
      },
      relations: {
        Item: {
          Product: true,
          Tray: {
            Storage: {
              Region: {
                Warehouse: true,
              },
            },
          },
        },
      },
    });

    return results;
  }
}
