import { AppDataSource } from '../data-source';
import { CRUD } from '../shared/interfaces/crud';
import { ProductCategory } from '../entity/ProductCategory';
import { IProductCategory } from '../shared/interfaces/IProductCategory';
import { EntityManager } from 'typeorm';
import { PagedList } from '../shared/models/paged-list.model';

export class ProductCategoryService implements CRUD {
  private productCategoryRepository;

  constructor() {
    this.productCategoryRepository = AppDataSource.getRepository(ProductCategory);
  }
  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async listFiltered(
    take: number,
    page: number,
    filterOptions: { CategoryId?: bigint },
  ): Promise<PagedList<ProductCategory[]>> {
    const skip = (page - 1) * take;

    const [data, total] = await this.productCategoryRepository.findAndCount({
      skip,
      take,
      where: [{ CategoryId: filterOptions.CategoryId }],
      relations: {
        Product: {
          ProductImages: true,
        },
      },
    });

    return {
      currentPage: page,
      totalLength: total,
      list: data,
    };
  }

  async list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async getAllProductCategories(): Promise<any> {}

  async create(resource: IProductCategory): Promise<void> {
    await this.productCategoryRepository.save(resource);
  }

  async deleteOneByIds(productId: bigint, categoryId: bigint): Promise<boolean> {
    await this.productCategoryRepository.delete({
      ProductId: productId,
      CategoryId: categoryId,
    });

    return true;
  }

  async deleteAllById(data: {
    productId: bigint;
    categoryId: bigint;
    transactionalEntityManager: EntityManager | null;
  }): Promise<void> {
    const { productId, categoryId, transactionalEntityManager } = data;

    const isProduct = productId !== BigInt(0);
    const productWhere = {
      ProductId: productId,
    };

    const categoryWhere = {
      CategoryId: categoryId,
    };

    if (transactionalEntityManager) {
      await transactionalEntityManager.delete(ProductCategory, isProduct ? productWhere : categoryWhere);
    } else {
      await this.productCategoryRepository.delete(isProduct ? productWhere : categoryWhere);
    }
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
