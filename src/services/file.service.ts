import { AppDataSource } from '../data-source';
import { ProductImage } from '../entity/ProductImage';
import { ProductManual } from '../entity/ProductManual';
import { PsaCheck } from '../entity/PsaCheck';
import { IProductImage } from '../shared/interfaces/IProductImage';
import { IProductManual } from '../shared/interfaces/IProductManual';
import { IPsaCheck } from '../shared/interfaces/IPsaCheck';
import { CRUD } from '../shared/interfaces/crud';
import fs from 'fs/promises';

class FileService implements CRUD {
  private productImageRepository;
  private productManualRepository;
  private psaCheckRepository;

  constructor() {
    this.productImageRepository = AppDataSource.getRepository(ProductImage);
    this.productManualRepository = AppDataSource.getRepository(ProductManual);
    this.psaCheckRepository = AppDataSource.getRepository(PsaCheck);
  }

  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async create(resource: IProductImage | IProductManual | IPsaCheck): Promise<void> {
    if ('ImageId' in resource) {
      const image = new ProductImage();
      image.ProductId = resource.ProductId;
      image.ImageId = resource.ImageId;

      await this.productImageRepository.insert(image);
    } else if ('ManualId' in resource) {
      const manual = new ProductManual();
      manual.ProductId = resource.ProductId;
      manual.ManualId = resource.ManualId;

      await this.productManualRepository.insert(manual);
    } else if ('PsaId' in resource) {
      const psaCheck = new PsaCheck();
      psaCheck.PsaId = resource.PsaId;
      psaCheck.ItemId = resource.ItemId;
      psaCheck.DateChecked = resource.DateChecked;

      await this.psaCheckRepository.insert(psaCheck);
    }
  }

  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async getFilesByProductId(id: bigint, type: string): Promise<ProductImage[] | ProductManual[] | null> {
    if (type === 'product-image') {
      return await this.productImageRepository.find({
        where: {
          ProductId: id,
        },
      });
    }

    if (type === 'product-manual') {
      return await this.productManualRepository.find({
        where: {
          ProductId: id,
        },
      });
    }

    return null;
  }

  async deleteById(productId: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async deleteFileById(fileId: string, fileType: string, parentIdentifier: bigint) {
    if (fileType === 'product-image') {
      await this.productImageRepository.manager.transaction(async (transactionalEntityManager) => {
        await transactionalEntityManager.delete(ProductImage, {
          ImageId: fileId,
        });
        await this.deleteFile(fileId, fileType, parentIdentifier);
      });
    } else if (fileType === 'product-manual') {
      await this.productManualRepository.manager.transaction(async (transactionalEntityManager) => {
        await transactionalEntityManager.delete(ProductManual, {
          ManualId: fileId,
        });
        await this.deleteFile(fileId, fileType, parentIdentifier);
      });
    } else if (fileType === 'psa-check') {
      await this.psaCheckRepository.manager.transaction(async (transactionalEntityManager) => {
        await transactionalEntityManager.delete(PsaCheck, {
          PsaId: fileId
        });
        await this.deleteFile(fileId, fileType, parentIdentifier);
      });
    }
  }

  async deleteDirectoryByIdentifier(parentIdentifier: bigint, fileType: string): Promise<void> {
    if (fileType === 'product-image') {
      await this.productImageRepository.manager.transaction(async (transactionalEntityManager) => {
        await transactionalEntityManager.delete(ProductImage, {
          ProductId: parentIdentifier,
        });
        await this.deleteDirectory(fileType, parentIdentifier);
      });
    } else if (fileType === 'product-manual') {
      await this.productManualRepository.manager.transaction(async (transactionalEntityManager) => {
        await transactionalEntityManager.delete(ProductManual, {
          ProductId: parentIdentifier,
        });
        await this.deleteDirectory(fileType, parentIdentifier);
      });
    } else if (fileType === 'psa-check') {
      await this.psaCheckRepository.manager.transaction(async (transactionalEntityManager) => {
        await transactionalEntityManager.delete(PsaCheck, {
          ItemId: parentIdentifier
        });
        await this.deleteDirectory(fileType, parentIdentifier);
      });
    }
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  private async deleteFile(fileId: string, fileType: string, parentIdentifier: bigint) {
    await fs.rm(`./uploads/${fileType}/${parentIdentifier}/${fileId}`, { recursive: false, force: true });

    const files = await fs.readdir(`./uploads/${fileType}/${parentIdentifier}`);
    if (files.length === 0) {
      await this.deleteDirectory(fileType, parentIdentifier);
    }
  }

  private async deleteDirectory(fileType: string, parentIdentifier: bigint) {
    await fs.rm(`./uploads/${fileType}/${parentIdentifier}`, { recursive: true, force: true });
  }
}

export default FileService;
