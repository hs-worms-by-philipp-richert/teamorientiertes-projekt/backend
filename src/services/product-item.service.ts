import { EntityManager } from 'typeorm';
import { AppDataSource } from '../data-source';
import { ProductItem } from '../entity/ProductItem';
import { IProductItem } from '../shared/interfaces/IProductItem';
import { CRUD } from '../shared/interfaces/crud';
import { LineItemService } from './line-item.service';

export class ProductItemService implements CRUD {
  private productItemRepository;
  private lineItemService;

  constructor() {
    this.productItemRepository = AppDataSource.getRepository(ProductItem);
    this.lineItemService = new LineItemService();
  }

  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }

  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  create(productItem: IProductItem): Promise<any> {
    return this.productItemRepository.insert(productItem);
  }

  async getAllByProductId(productId: bigint) {
    return this.productItemRepository.find({
      select: {
        ItemId: true,
        QrCode: true,
        Serialnumber: true,
        ProductId: true,
        VariantId: true,
        TrayId: true,
        Archived: true,
        ExpiresInYears: true,
        IsDefect: true,
        IsPsa: true,
        ManufacturingDate: true,
      },
      where: {
        ProductId: productId,
      },
      relations: {
        Tray: true,
      },
    });
  }

  async putById(id: bigint, productItem: IProductItem): Promise<boolean> {
    const result = await this.productItemRepository.update(
      { ItemId: id },
      {
        ProductId: productItem.ProductId,
        VariantId: productItem.VariantId,
        TrayId: productItem.TrayId,
        ManufacturingDate: productItem.ManufacturingDate,
        ExpiresInYears: productItem.ExpiresInYears,
        IsPsa: productItem.IsPsa,
        IsDefect: productItem.IsDefect,
        QrCode: productItem.QrCode,
        Serialnumber: productItem.Serialnumber,
        Archived: productItem.Archived,
      },
    );
    return result.affected !== 0;
  }

  async readById(id: bigint): Promise<ProductItem | null> {
    return await this.productItemRepository.findOne({
      where: { ItemId: id },
    });
  }

  async deleteById(id: bigint): Promise<boolean> {
    const foundLineItems = await this.lineItemService.readByProductItemId(id);
    if (foundLineItems.length > 0) {
      const result = await this.productItemRepository.update({ ItemId: id }, { Archived: true });

      return result.affected !== 0;
    }
    const result = await this.productItemRepository.delete({ ItemId: id });
    return result.affected !== 0;
  }

  async deleteByIdTransaction(id: bigint, transactionalEntityManager: EntityManager): Promise<boolean> {
    const foundLineItems = await this.lineItemService.readByProductItemId(id);

    if (foundLineItems.length > 0) {
      await this.productItemRepository.update({ ItemId: id }, { Archived: true });
      return true;
    }
    await transactionalEntityManager.delete(ProductItem, { ItemId: id });
    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
