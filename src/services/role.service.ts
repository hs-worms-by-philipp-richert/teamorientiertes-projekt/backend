import { Repository } from 'typeorm';
import { Role } from '../entity/Role';
import { AppDataSource } from '../data-source';
import { CRUD } from '../shared/interfaces/crud';
import { Permission } from '../shared/enums/permissions.enum';

export class RoleService implements CRUD {
  private roleRepository: Repository<Role>;

  constructor() {
    this.roleRepository = AppDataSource.getRepository(Role);
  }
  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  create(resource: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async getByPermission(permission: Permission) {
    return await this.roleRepository.findOne({ where: { Permission: permission } });
  }

  async list(): Promise<{ data: Role[]; total: number }> {
    const [data, total] = await this.roleRepository.findAndCount({
      order: { Name: 'ASC' },
    });

    return { data, total };
  }
}
