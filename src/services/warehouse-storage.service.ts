import { AppDataSource } from '../data-source';
import { WarehouseStorage } from '../entity/WarehouseStorage';
import { ErrorType } from '../shared/enums/error-types.enum';
import { IWarehouseStorage } from '../shared/interfaces/IWarehouseStorage';
import { CRUD } from '../shared/interfaces/crud';

class WarehouseStorageService implements CRUD {
  private warehouseStorageRepository;

  constructor() {
    this.warehouseStorageRepository = AppDataSource.getRepository(WarehouseStorage);
  }

  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async list(take: number, page: number): Promise<any> {
    const skip: number = (page - 1) * take;

    const [data, totalLength] = await this.warehouseStorageRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        Region: true,
        Trays: true,
      },
      order: { Name: 'ASC' },
    });

    return { data, totalLength };
  }

  async create(data: IWarehouseStorage): Promise<boolean> {
    const { RegionId: regionId, Name: name } = data;

    let newWarehouseStorage = new WarehouseStorage();
    newWarehouseStorage.Name = name;
    newWarehouseStorage.RegionId = regionId;

    await this.warehouseStorageRepository.insert(newWarehouseStorage);

    return true;
  }

  async putById(id: bigint, resources: IWarehouseStorage): Promise<boolean> {
    const result = await this.warehouseStorageRepository.update({ StorageId: id }, resources);

    return result.affected !== 0;
  }

  async readById(id: any): Promise<WarehouseStorage | null> {
    const bigIntId = BigInt(id);

    if (!bigIntId) throw ErrorType.InvalidInput;

    const foundStorage: WarehouseStorage | null = await this.warehouseStorageRepository.findOne({
      where: {
        StorageId: bigIntId,
      },
      relations: {
        Trays: true,
      },
    });

    return foundStorage;
  }

  async readAllByRegionId(regionId: bigint): Promise<WarehouseStorage[]> {
    const storages: WarehouseStorage[] = await this.warehouseStorageRepository.find({
      where: {
        RegionId: regionId,
      },
      order: {
        Name: 'ASC',
      },
    });

    return storages;
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.warehouseStorageRepository.delete({ StorageId: id });

    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}

export default WarehouseStorageService;
