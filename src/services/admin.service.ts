import { AppDataSource } from '../data-source';
import { User } from '../entity/User';
import { CRUD } from '../shared/interfaces/crud';

export class AdminService implements CRUD {
  private userBuilder = AppDataSource.getRepository(User);

  async list(take: number, page: number): Promise<any> {
    const skip = (page - 1) * take;
    const [data, total] = await this.userBuilder.findAndCount({
      skip: skip,
      take: take,
      relations: {
        Orders: {
          OrderId: true,
        },
        Role: true,
      },
      order: { Firstname: 'ASC' },
    });

    return { data, total };
  }

  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  create(resource: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  async readById(id: any): Promise<User | null> {
    const user = await this.userBuilder.findOneBy({
      UserId: id,
    });

    return user;
  }

  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
