import fs from 'fs';
import { AppDataSource } from '../data-source';
import { User } from '../entity/User';
import { IUser } from '../shared/interfaces/IUser';
import { AuthPayload } from '../shared/models/auth-payload.model';
import bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';
import jwt, { Secret, SignOptions } from 'jsonwebtoken';
import { Session } from '../entity/Session';
import { AccessToken } from '../shared/models/access-token.model';
import { RefreshToken } from '../shared/models/refresh-token.model';
import { LessThan } from 'typeorm';
import { ErrorType } from '../shared/enums/error-types.enum';
import { Permission } from '../shared/enums/permissions.enum';
import { Role } from '../entity/Role';
import PermissionHelper from '../shared/helper/permission.helper';

class AuthService {
  private accessTokenOptions: SignOptions;
  private refreshTokenOptions: SignOptions;
  private privateKey: Secret;
  private publicKey: Secret;
  private saltRounds: number;

  private userRepository;
  private sessionRepository;
  private roleRepository;

  constructor() {
    this.accessTokenOptions = {
      algorithm: 'RS256',
      expiresIn: '5m',
    };

    this.refreshTokenOptions = {
      algorithm: 'RS256',
      expiresIn: '6h',
    };

    this.privateKey = fs.readFileSync('./keys/jwtRS256.key');
    this.publicKey = fs.readFileSync('./keys/jwtRS256.key.pub');

    this.saltRounds = 11;

    this.userRepository = AppDataSource.getRepository(User);
    this.sessionRepository = AppDataSource.getRepository(Session);
    this.roleRepository = AppDataSource.getRepository(Role);
  }

  parsePermissions(octalValue: number | undefined): Permission[] {
    return PermissionHelper.parsePermissions(octalValue);
  }

  sumPermissions(permissionList: Permission[]): number {
    return PermissionHelper.sumPermissions(permissionList);
  }

  isAllowedTo(permission: Permission, userPermissions: Permission[]): boolean {
    return PermissionHelper.isAllowedTo(permission, userPermissions);
  }

  async login(identifier: string, password: string): Promise<AuthPayload> {
    const identifierConverted = identifier.replace(/\//g, '-');

    const user = await this.userRepository.findOne({
      where: [{ Email: identifierConverted }, { UserId: identifierConverted }],
      relations: {
        Role: true
      },
      select: {
        Firstname: true,
        Lastname: true,
        UserId: true,
        Street: true,
        Postal: true,
        City: true,
        Housenumber: true,
        Country: true,
        Email: true,
        Phone: true,
        IsVerified: true,
        Password: true,
        RoleId: true
      }
    });

    if (!user || !user.Password) {
      throw ErrorType.WrongCredentials;
    }

    const compareCrypt = await bcrypt.compare(password, user.Password);

    if (!compareCrypt) {
      throw ErrorType.WrongCredentials;
    }

    if (!user.IsVerified) {
      throw ErrorType.UserNotVerified;
    }

    const userWithoutPassword: IUser = {
      Firstname: user.Firstname,
      Lastname: user.Lastname,
      UserId: user.UserId,
      Street: user.Street,
      Postal: user.Postal,
      City: user.City,
      Housenumber: user.Housenumber,
      Country: user.Country,
      Email: user.Email,
      Phone: user.Phone,
      IsVerified: user.IsVerified,
      RoleId: user.RoleId,
      Role: user.Role
    };

    const authPayload: AuthPayload = await this.generateTokenPair(userWithoutPassword);

    return authPayload;
  }

  async hashPassword(input: string): Promise<string> {
    return await bcrypt.hash(input, this.saltRounds);
  }

  async register(user: IUser, createdByAdmin: boolean): Promise<ErrorType | undefined> {
    if (!user.Password || !user.UserId) return ErrorType.InvalidInput;

    user.UserId = user.UserId.replace(/\//g, '-');

    // check userId?
    const checkUserId = await this.userRepository.findOne({
      where: { UserId: user.UserId },
    });
    if (checkUserId !== null) return ErrorType.UserIdAlreadyTaken;

    // check if email exists
    const checkEmail = await this.userRepository.findOne({
      where: { Email: user.Email },
    });
    if (checkEmail !== null) return ErrorType.EmailAlreadyTaken;

    // hash password
    const hashedPassword = await this.hashPassword(user.Password);

    const getLowestRole = await this.roleRepository.findOne({ where: { Permission: 0 } });

    if (!getLowestRole) {
      throw ErrorType.NoFittingRoleAvailable;
    }

    // insert user
    const newUser = new User();
    newUser.Firstname = user.Firstname;
    newUser.Lastname = user.Lastname;
    newUser.UserId = user.UserId;
    newUser.Street = user.Street;
    newUser.Postal = user.Postal;
    newUser.City = user.City;
    newUser.Housenumber = user.Housenumber;
    newUser.Country = user.Country;
    newUser.Email = user.Email;
    newUser.Phone = user.Phone;
    newUser.Password = hashedPassword;
    newUser.RoleId = getLowestRole.RoleId;
    newUser.IsVerified = true;

    await this.userRepository.insert(newUser);
  }

  async generateTokenPair(user: IUser, sessionId: string = uuidv4()): Promise<AuthPayload> {
    const tokenId: string = uuidv4();

    if (!user.UserId) {
      throw 'missing_userid';
    }

    const accessToken = jwt.sign(
      {
        user,
        sessionId,
        associatedRefreshToken: tokenId
      },
      this.privateKey,
      this.accessTokenOptions
    );

    const refreshToken = jwt.sign(
      {
        sessionId,
        tokenId,
        userId: user.UserId
      },
      this.privateKey,
      this.refreshTokenOptions
    );

    // insert tokenId and sessionId to db
    const sessionObj: Session = {
      UserId: user.UserId,
      SessionId: sessionId,
      TokenId: tokenId,
      CreatedAt: BigInt(Date.now())
    };

    await this.sessionRepository.insert(sessionObj);

    return { accessToken, refreshToken };
  }

  async refreshToken(accessToken: string, refreshToken: string) {
    try {
      // try to decode access and refresh token and compare tokenId
      const decodeAccessToken = this.decode<AccessToken>(accessToken);
      const decodeRefreshToken = this.decode<RefreshToken>(refreshToken);

      if (decodeAccessToken.associatedRefreshToken !== decodeRefreshToken.tokenId)
        throw 'token_pair_not_matching';

      // try to verify access token
      const verifyAccessToken = this.verify<AccessToken>(accessToken);

      if (!verifyAccessToken) {
        // try to verify refresh token
        const verifyRefreshToken = this.verify<RefreshToken>(refreshToken);

        if (!verifyRefreshToken)
          // no valid refresh token
          throw 'refresh_token_invalid';

        // search for sessionId and userId in database
        const refreshTokenFamily = await this.sessionRepository.find({
          where: {
            UserId: verifyRefreshToken.userId,
            SessionId: verifyRefreshToken.sessionId
          },
          order: {
            CreatedAt: 'DESC'
          },
        });

        if (refreshTokenFamily.length === 0)
          // no session found
          throw 'refresh_token_not_found';

        // look for token in db-result array and get index (token MUST be first element, if it isn't, the token has been compromised)
        const findLastTokenIndex = refreshTokenFamily.findIndex((m) => m.TokenId === verifyRefreshToken.tokenId);
        if (findLastTokenIndex > 0) throw 'refresh_token_compromised';

        // get user data for new token
        const user = await this.userRepository.findOne({
          where: {
            UserId: verifyRefreshToken.userId
          },
          relations: {
            Role: true
          },
        });

        if (!user)
          // user doesn't exist (anymore)
          throw 'user_gone';

        const { Password, ...userWithoutPassword }: IUser = user;

        // create new access and refresh token
        const tokengen = await this.generateTokenPair(userWithoutPassword, verifyRefreshToken.sessionId);

        // delete old token (older than 7 days)
        await this.sessionRepository.delete({
          CreatedAt: LessThan<bigint>(BigInt(Date.now() - 1000 * 60 * 60 * 24 * 7)),
        });

        return {
          refreshed: true,
          accessToken: tokengen.accessToken,
          refreshToken: tokengen.refreshToken
        };
      } else {
        // access token is valid, proceed with request
        return {
          refreshed: false,
          accessToken,
          refreshToken
        };
      }
    } catch (e) {
      if (typeof e === 'string') {
        if (
          e === 'refresh_token_invalid' ||
          e === 'refresh_token_not_found' ||
          e === 'refresh_token_compromised' ||
          e === 'token_pair_not_matching' ||
          e === 'user_gone'
        ) {
          const decodedRefreshToken = this.decode<RefreshToken>(refreshToken);

          if (!decodedRefreshToken) throw 'refresh_token_invalid';

          // delete token family (kill them all HAHA)
          await this.sessionRepository.delete({
            SessionId: decodedRefreshToken.sessionId,
            UserId: decodedRefreshToken.userId
          });
        }
      }

      throw e;
    }
  }

  verify<T>(token: string): T | null {
    try {
      return jwt.verify(token, this.publicKey) as T;
    } catch {
      return null;
    }
  }

  decode<T>(token: string): T {
    return jwt.decode(token) as T;
  }

  randomPasswordGenerator(): string {
    return Math.random().toString(36).slice(-8);
  }
}

export default new AuthService();
