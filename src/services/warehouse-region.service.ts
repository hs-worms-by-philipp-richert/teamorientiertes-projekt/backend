import { AppDataSource } from '../data-source';
import { WarehouseRegion } from '../entity/WarehouseRegion';
import { IWarehouseRegion } from '../shared/interfaces/IWarehouseRegion';
import { CRUD } from '../shared/interfaces/crud';

class WarehouseRegionService implements CRUD {
  private warehouseRegionRepository;

  constructor() {
    this.warehouseRegionRepository = AppDataSource.getRepository(WarehouseRegion);
  }

  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  async list(take: number, page: number): Promise<any> {
    const skip: number = (page - 1) * take;

    const [data, totalLength] = await this.warehouseRegionRepository.findAndCount({
      skip: skip,
      take: take,
      relations: {
        Storages: {
          Trays: true,
        },
        Warehouse: true,
      },
      order: { Name: 'ASC' },
    });

    return { data, totalLength };
  }

  async create(data: IWarehouseRegion): Promise<boolean> {
    const { WarehouseId: warehouseId, Name: name } = data;

    let newWarehouseRegion = new WarehouseRegion();
    newWarehouseRegion.Name = name;
    newWarehouseRegion.WarehouseId = warehouseId;

    await this.warehouseRegionRepository.insert(newWarehouseRegion);

    return true;
  }

  async putById(id: bigint, resources: IWarehouseRegion): Promise<boolean> {
    const result = await this.warehouseRegionRepository.update({ RegionId: id }, resources);

    return result.affected !== 0;
  }

  async readById(id: any): Promise<WarehouseRegion | null> {
    const bigIntId = BigInt(id);

    const foundRegion: WarehouseRegion | null = await this.warehouseRegionRepository.findOne({
      where: {
        RegionId: bigIntId,
      },
    });

    return foundRegion;
  }

  async readAllByWarehouseId(warehouseId: bigint): Promise<WarehouseRegion[]> {
    const warehouses: WarehouseRegion[] = await this.warehouseRegionRepository.find({
      where: {
        WarehouseId: warehouseId,
      },
      order: {
        Name: 'ASC',
      },
      relations: {
        Storages: {
          Trays: true,
        },
      },
    });

    return warehouses;
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.warehouseRegionRepository.delete({ RegionId: id });

    return true;
  }

  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}

export default WarehouseRegionService;
