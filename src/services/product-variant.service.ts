import { AppDataSource } from '../data-source';
import { CRUD } from '../shared/interfaces/crud';
import { IProductVariant } from '../shared/interfaces/IProductVariant';
import { ProductVariant } from '../entity/ProductVariant';
import { ProductVariantPropertyService } from './product-variant-property.service';
import { EntityManager } from 'typeorm';

export class ProductVariantService implements CRUD {
  private productVariantRepository;
  private productVariantPropertyService: ProductVariantPropertyService;

  constructor() {
    this.productVariantRepository = AppDataSource.getRepository(ProductVariant);
    this.productVariantPropertyService = new ProductVariantPropertyService();
  }
  async putById(id: bigint, resource: IProductVariant): Promise<boolean> {
    await this.productVariantRepository.update({ VariantId: id }, resource);
    return true;
  }
  async readById(variantId: bigint): Promise<ProductVariant | null> {
    return await this.productVariantRepository.findOne({
      where: {
        VariantId: variantId,
      },
    });
  }
  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  async list(take: number, page: number): Promise<any> {
    const skip = (page - 1) * take;

    const [result, total] = await this.productVariantRepository.findAndCount({
      skip: skip,
      take: take,
      order: { VariantId: 'ASC' },
      relations: {
        ProductId: true,
        Properties: true,
      },
    });

    return { result, total };
  }

  async getAllByProductId(id: bigint) {
    return await this.productVariantRepository.find({
      where: {
        ProductId: id,
      },
    });
  }

  async create(resource: IProductVariant): Promise<void> {
    await this.productVariantRepository.save(resource);
  }

  async deleteById(id: bigint): Promise<boolean> {
    await this.productVariantRepository.manager.transaction(async (transactionalEntityManager) => {
      this.productVariantPropertyService.deleteAllByVariantId(id, transactionalEntityManager);

      await transactionalEntityManager.delete(ProductVariant, {
        VariantId: id,
      });
    });
    return true;
  }

  async deleteByProductId(data: { productId: bigint; transactionalEntityManager: EntityManager }) {
    const { productId, transactionalEntityManager } = data;
    const foundVariants = await transactionalEntityManager.find(ProductVariant, {
      where: {
        ProductId: productId,
      },
    });
    if (foundVariants.length > 0) {
      for (const item of foundVariants) {
        await this.productVariantPropertyService.deleteAllByVariantId(item.VariantId, transactionalEntityManager);
      }
      await transactionalEntityManager.delete(ProductVariant, { ProductId: productId });
    }
  }

  async patchById(id: bigint, description: string): Promise<boolean> {
    await this.productVariantRepository.update({ VariantId: id }, { Description: description });

    return true;
  }
}
