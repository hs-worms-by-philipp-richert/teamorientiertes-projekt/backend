import { Router } from 'express';
import { getAllRoles } from '../controller/role.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/', authRestrictiveMiddleware, AuthClearance.allowAdmin, getAllRoles);

export default router;
