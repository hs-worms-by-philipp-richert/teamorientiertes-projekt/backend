import { Router } from 'express';
import { getFile, multerErrorHandlingMiddleware, uploadFile, deleteFilesByProductId, deleteFileById, uploadPreconditionMiddleware } from '../controller/file.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

const allowedTypes = 'product-image|product-manual|psa-check';

router.post(`/upload/:uploadType(${allowedTypes})/:parentIdentifier`, authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, uploadPreconditionMiddleware, multerErrorHandlingMiddleware, uploadFile);
router.get(`/:fileType(${allowedTypes})/:parentIdentifier/:fileId`, getFile);
router.delete(`/:fileType(product-image|product-manual)/:id`, authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteFilesByProductId);
router.delete(`/:fileType(${allowedTypes})/:parentIdentifier/:fileId`, authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteFileById);

export default router;
