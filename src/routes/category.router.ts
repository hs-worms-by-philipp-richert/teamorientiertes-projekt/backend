import { Router } from 'express';
import {
  createCategory,
  deleteCategoryById,
  getCategoryById,
  getFilteredCategoryList,
  updateCategoryById,
} from '../controller/category.controller';

const router = Router();

router.get('/list', getFilteredCategoryList);
router.get('/:id', getCategoryById);
router.post('/', createCategory);
router.put('/:id', updateCategoryById);
router.delete('/:id', deleteCategoryById);

export default router;
