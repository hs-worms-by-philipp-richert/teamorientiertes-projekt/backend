import { Router } from 'express';
import {
  createProductItem,
  deleteProductItemById,
  getProductItemById,
  getProductItemsByProductId,
  updateProductItemById,
} from '../controller/product-item.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/product/:id', getProductItemsByProductId);
router.get('/:id', getProductItemById);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createProductItem);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, updateProductItemById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteProductItemById);

export default router;
