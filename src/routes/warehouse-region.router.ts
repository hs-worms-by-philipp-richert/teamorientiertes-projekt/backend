import { Router } from 'express';
import {
  createRegion,
  deleteRegionById,
  getRegionById,
  getRegionList,
  getRegionsByWarehouseId,
  updateRegionById,
} from '../controller/warehouse-region.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/list', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getRegionList);
router.get('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getRegionById);
router.get('/warehouse/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getRegionsByWarehouseId);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createRegion);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, updateRegionById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteRegionById);

export default router;
