import { Router } from 'express';
import { getProductItemsWithPsa, getPsaListByItemId } from '../controller/psa.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/product-item/list', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getProductItemsWithPsa);
router.get('/list/:itemId', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getPsaListByItemId);

export default router;
