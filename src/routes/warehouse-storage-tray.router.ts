import { Router } from 'express';
import {
  createTray,
  deleteTrayById,
  getTrayById,
  getTrayList,
  getTraysByStorageId,
  updateTrayById,
} from '../controller/warehouse-storage-tray.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/list', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getTrayList);
router.get('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getTrayById);
router.get('/storage/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getTraysByStorageId);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createTray);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, updateTrayById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteTrayById);

export default router;
