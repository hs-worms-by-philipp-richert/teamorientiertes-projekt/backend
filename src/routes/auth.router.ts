import { Router } from 'express';
import {
  AuthClearance,
  authRefreshMiddleware,
  authRestrictiveMiddleware,
  getAuthRefresh,
  postLogin,
  testEndpoint,
} from '../controller/auth.controller';

const router = Router();

router.post('/login', postLogin);
router.get('/refresh', getAuthRefresh)
router.get('/test', authRestrictiveMiddleware, AuthClearance.allowAdmin, testEndpoint);

export default router;
