import { Router } from 'express';
import {
  changeOrderStatus,
  changeOrderStatusFinalizePickUp,
  createOrder,
  deleteOrderById,
  getOrderById,
  getOrderList,
  getOrdersByUserId,
} from '../controller/order.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/list', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getOrderList);
router.get('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getOrderById);
router.get('/user/:userId', authRestrictiveMiddleware, AuthClearance.allowMember, getOrdersByUserId);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowMember, createOrder);
router.put('/:id/status', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, changeOrderStatus);
router.put('/:id/status/finalize-pickup', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, changeOrderStatusFinalizePickUp);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteOrderById);

export default router;
