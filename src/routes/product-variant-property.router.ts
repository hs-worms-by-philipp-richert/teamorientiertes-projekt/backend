import { Router } from 'express';
import {
  createVariantProperty,
  deleteVariantPropertyById,
  getAllByVariantId,
} from '../controller/product-variant-property.controller';
import { updateProductVariantById } from '../controller/product-variant.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/:id', getAllByVariantId);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createVariantProperty);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, updateProductVariantById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteVariantPropertyById);

export default router;
