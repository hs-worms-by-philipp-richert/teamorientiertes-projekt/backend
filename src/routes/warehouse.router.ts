import { Router } from 'express';
import {
  createWarehouse,
  getWarehousesList,
  deleteById,
  getWarehouseByID,
  updateWarehouseById,
} from '../controller/warehouse.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/list', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getWarehousesList);
router.get('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getWarehouseByID);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowAdmin, createWarehouse);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowAdmin, updateWarehouseById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowAdmin, deleteById);

export default router;
