import { Router } from 'express';
import {
  createProductVariant,
  getAllByProductId,
  getAllByVariantId,
  updateProductVariantById,
  deleteProductVariantById
} from '../controller/product-variant.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/product/:id', getAllByProductId);
router.get('/:id', getAllByVariantId);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createProductVariant);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, updateProductVariantById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteProductVariantById);

export default router;
