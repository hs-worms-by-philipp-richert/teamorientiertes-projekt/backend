import { Router } from 'express';
import {
  createProductCategory,
  deleteAllProductCategoriesByCategoryId,
  deleteByIds,
  deleteAllProductCategoriesById,
} from '../controller/product-category.controller';

import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createProductCategory);
router.delete('/product/:productId', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteAllProductCategoriesById);
router.delete('/category/:categoryId', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteAllProductCategoriesByCategoryId);
router.delete('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteByIds);

export default router;
