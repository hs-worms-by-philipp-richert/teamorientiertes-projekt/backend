import { Router } from 'express';
import {
  createStorage,
  deleteStorageById,
  getStorageById,
  getStorageList,
  getStoragesByRegionId,
  updateStorageById,
} from '../controller/warehouse-storage.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/list', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getStorageList);
router.get('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getStorageById);
router.get('/region/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, getStoragesByRegionId);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createStorage);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, updateStorageById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteStorageById);

export default router;
