import { Router } from 'express';
import {
  createLineItem,
  deleteLineItemById,
  setReturnDateById,
  setReturnDateToNullById,
  updateLineItemById,
} from '../controller/line-item.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.post('/', authRestrictiveMiddleware, AuthClearance.allowMember, createLineItem);
router.put('/set-return-date/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, setReturnDateById);
//router.put('/:id', updateLineItemById);
router.put('/set-return-date-to-null/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, setReturnDateToNullById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteLineItemById);

export default router;
