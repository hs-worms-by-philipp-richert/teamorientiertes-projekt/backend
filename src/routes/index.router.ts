import { Router } from 'express';
import productRoutes from './product.router';
import orderRoutes from './order.router';
import categoryRoutes from './category.router';
import authRoutes from './auth.router';
import warehouseRoutes from './warehouse.router';
import warehouseRegionRoutes from './warehouse-region.router';
import warehouseStorageRoutes from './warehouse-storage.router';
import warehouseStorageTrayRoutes from './warehouse-storage-tray.router';
import userRoutes from './user.router';
import productItemRoutes from './product-item.router';
import productVariantRoutes from './product-variant.router';
import productVariantPropertiesRoutes from './product-variant-property.router';
import fileRoutes from './file.router';
import productCategoryRoutes from './product-category.router';
import roleRoutes from './role.router';
import lineItemRoutes from './line-item.router';
import pdfRoutes from './pdf.router';
import eMailRoutes from './email.router';
import psaRoutes from './psa.router';
import { authDecodeUserMiddleware } from '../controller/auth.controller';

const indexRouter = Router();

indexRouter.use(authDecodeUserMiddleware);

indexRouter.use('/product', productRoutes);
indexRouter.use('/product-variant', productVariantRoutes);
indexRouter.use('/product-variant-property', productVariantPropertiesRoutes);
indexRouter.use('/product-category', productCategoryRoutes);
indexRouter.use('/order', orderRoutes);
indexRouter.use('/category', categoryRoutes);

indexRouter.use('/auth', authRoutes);
indexRouter.use('/warehouse', warehouseRoutes);
indexRouter.use('/warehouse-region', warehouseRegionRoutes);
indexRouter.use('/warehouse-storage', warehouseStorageRoutes);

indexRouter.use('/warehouse-storage-tray', warehouseStorageTrayRoutes);
indexRouter.use('/user', userRoutes);
indexRouter.use('/product-item', productItemRoutes);
indexRouter.use('/file', fileRoutes);

indexRouter.use('/role', roleRoutes);
indexRouter.use('/line-item', lineItemRoutes);
indexRouter.use('/pdf', pdfRoutes);
indexRouter.use('/email', eMailRoutes);
indexRouter.use('/psa', psaRoutes)

export default indexRouter;
