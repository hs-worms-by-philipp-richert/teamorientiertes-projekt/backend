import { Router } from 'express';
import {
  getProductById,
  getProductList,
  createProduct,
  deleteProductById,
  updateProductById,
  getProductAvailability,
} from '../controller/product.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/list', getProductList);
router.get('/:id', getProductById);
router.get('/:productId/availability/:startDate/:endDate', getProductAvailability);
router.post('/', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, createProduct);
router.put('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, updateProductById);
router.delete('/:id', authRestrictiveMiddleware, AuthClearance.allowWarehouseWorker, deleteProductById);

export default router;
