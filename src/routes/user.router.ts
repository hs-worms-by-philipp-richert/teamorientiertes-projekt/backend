import { Router } from 'express';
import { postRegistration } from '../controller/user.controller';
import {
  createUser,
  deleteUser,
  getUser,
  getUserList,
  requestRoleUpdate,
  updateToInternalById,
  updateUser,
  updateUserSelf,
} from '../controller/admin.controller';
import { AuthClearance, authRestrictiveMiddleware } from '../controller/auth.controller';

const router = Router();

router.get('/list',  authRestrictiveMiddleware, AuthClearance.allowAdmin, getUserList);

router.put('/request-role-update', authRestrictiveMiddleware, AuthClearance.allowMember, requestRoleUpdate);

router.put('/update-role-to-internal/:userId', authRestrictiveMiddleware, AuthClearance.allowAdmin, updateToInternalById);

router.get('/:userId', authRestrictiveMiddleware, AuthClearance.allowAdmin, getUser);

router.put('/:userId', authRestrictiveMiddleware, AuthClearance.allowAdmin, updateUser);

router.put('/', authRestrictiveMiddleware, AuthClearance.allowMember, updateUserSelf);

router.delete('/:userId', authRestrictiveMiddleware, AuthClearance.allowAdmin, deleteUser);

router.post('/register', postRegistration);

router.post('/create', authRestrictiveMiddleware, AuthClearance.allowAdmin, createUser);

export default router;
