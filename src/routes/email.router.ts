import { Router } from 'express';
import { createOrderInvoice, createReadyForPickup, orderExceeded } from '../controller/email.controller';

const router = Router();

router.post('/order-invoice/:orderId', createOrderInvoice);
router.post('/order-ready-for-pickup/:orderId', createReadyForPickup);
router.post('/order-exceeded/:orderId', orderExceeded);

export default router;
