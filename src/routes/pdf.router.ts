import { Router } from 'express';
import { generatePDF, getPDF } from '../controller/pdf.controller';

const router = Router();

router.post('/:orderId', generatePDF);
router.get('/:orderId', getPDF);

export default router;
