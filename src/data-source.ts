import 'reflect-metadata';
import { DataSource } from 'typeorm';
import dotenv from 'dotenv';

// import entities
import { User } from './entity/User';
import { Order } from './entity/Order';
import { LineItem } from './entity/LineItem';
import { Product } from './entity/Product';
import { ProductItem } from './entity/ProductItem';
import { ProductVariant } from './entity/ProductVariant';
import { Warehouse } from './entity/Warehouse';
import { WarehouseStorage } from './entity/WarehouseStorage';
import { WarehouseStorageTray } from './entity/WarehouseStorageTray';
import { ProductImage } from './entity/ProductImage';
import { ProductManual } from './entity/ProductManual';
import { Role } from './entity/Role';
import { Category } from './entity/Category';
import { ProductCategory } from './entity/ProductCategory';
import { ProductVariantProperty } from './entity/ProductVariantProperty';
import { WarehouseRegion } from './entity/WarehouseRegion';
import { Session } from './entity/Session';
import { PsaCheck } from './entity/PsaCheck';

dotenv.config();

const host: string = process.env.DB_HOST ?? '';
const username: string = process.env.DB_USERNAME ?? '';
const password: string = process.env.DB_PASSWORD ?? '';
const database: string = process.env.DB_DATABASE ?? '';

const isProduction: boolean = process.env.NODE_ENV == 'production' ? true : false;

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: host,
  port: Number(process.env.DB_PORT ?? 5432),
  username: username,
  password: password,
  database: database,
  synchronize: !isProduction,
  logging: !isProduction,
  entities: [
    Category,
    LineItem,
    Order,
    Product,
    ProductCategory,
    ProductImage,
    ProductItem,
    ProductManual,
    ProductVariant,
    ProductVariantProperty,
    PsaCheck,
    Role,
    Session,
    User,
    Warehouse,
    WarehouseRegion,
    WarehouseStorage,
    WarehouseStorageTray,
  ],
  subscribers: [],
  migrations: [],
});
