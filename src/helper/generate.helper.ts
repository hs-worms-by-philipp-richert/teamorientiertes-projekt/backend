export class GenerateHelper {
  public generateRegExp(inputString: string): RegExp {
    const pattern = inputString
      .split('')
      .map((char) => {
        return `[${char.toLocaleLowerCase()}${char.toUpperCase()}}]`;
      })
      .join('');

    return new RegExp(pattern);
  }

  public formatUpperString(input: string): string {
    if (input.length === 0) {
      return input;
    }

    const firstLetter = input.charAt(0).toUpperCase();
    const restLetter = input.slice(1);

    return firstLetter + restLetter;
  }

  public firstUpperString(input: string): string {
    if (input.length === 0) {
      return input;
    }
    const firstLetter = input.charAt(0).toUpperCase();
    const restLetter = input.slice(1).toLowerCase();

    return firstLetter + restLetter;
  }

  public camelCase(input: string): string {
    return input
      .replace(/\s(.)/g, ($1) => {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, ($1) => {
        return $1.toLowerCase();
      });
  }
}
