import { ValueTransformer } from 'typeorm';
import { OrderStatus } from '../shared/enums/order-status.enum';
import { Permission } from '../shared/enums/permissions.enum';

export const bigint: ValueTransformer = {
  to: (entityValue: number) => entityValue,
  from: (databaseValue: string): number => parseInt(databaseValue, 10),
};

export const number: ValueTransformer = {
  to: (entityValue: number) => entityValue,
  from: (databaseValue: string): number => Number(databaseValue),
};

export const orderStatus: ValueTransformer = {
  to: (entityValue: OrderStatus) => entityValue,
  from: (databaseValue: string): OrderStatus => parseInt(databaseValue),
};

export const permissionTransformer: ValueTransformer = {
  to: (entityValue: Permission) => entityValue,
  from: (databaseValue: string): Permission => parseInt(databaseValue),
};

export const dateTransformer: ValueTransformer = {
  to: (entityValue: Date | null) => entityValue,
  from: (databaseValue: string): Date | null => (databaseValue ? new Date(databaseValue) : null)
};
