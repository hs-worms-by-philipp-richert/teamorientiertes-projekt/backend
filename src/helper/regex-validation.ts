export default class RegExValidation {
  static userIdExp: RegExp = /289\/00\/[0-9]{6}/; // validates 289/00/XXXXXX

  static validate(input: string | undefined, regex: RegExp): boolean {
    if (!input)
      return false;

    const valid = regex.test(input);

    return valid;
  }
}