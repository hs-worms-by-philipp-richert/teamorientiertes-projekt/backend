import fs from 'fs';
import multer, { MulterError } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { ErrorType } from '../shared/enums/error-types.enum';

const allowedImageTypes = ['png', 'jpg', 'jpeg', 'webp'];
const allowedImageMimes = ['image/png', 'image/jpg', 'image/jpeg', 'image/webp'];
const allowedManualTypes = ['pdf'];
const allowedManualMimes = ['application/pdf'];
const allowedPsaCheckTypes = ['pdf'];
const allowedPsaCheckMimes = ['application/pdf'];

export const storage = multer.diskStorage({
  async destination(req, file, callback) {
    const { uploadType, parentIdentifier } = req.params;
    const dir = `./uploads/${uploadType}/${parentIdentifier}`;
    const extension = file.originalname.match(/\.([a-zA-Z]+)$/)?.[1];

    if (uploadType != 'product-image' && uploadType != 'product-manual' && uploadType != 'psa-check') {
      return callback(new MulterError('LIMIT_FIELD_KEY', ErrorType.InvalidUploadType), dir);
    }

    if (uploadType == 'product-image' && !allowedImageTypes.includes(extension ?? '') && !allowedImageMimes.includes(file.mimetype)) {
      return callback(new MulterError('LIMIT_UNEXPECTED_FILE', ErrorType.InvalidFileType), dir);
    }

    if (uploadType == 'product-manual' && !allowedManualTypes.includes(extension ?? '') && !allowedManualMimes.includes(file.mimetype)) {
      return callback(new MulterError('LIMIT_UNEXPECTED_FILE', ErrorType.InvalidFileType), dir);
    }

    if (uploadType == 'psa-check' && !allowedPsaCheckTypes.includes(extension ?? '') && !allowedPsaCheckMimes.includes(file.mimetype)) {
      return callback(new MulterError('LIMIT_UNEXPECTED_FILE', ErrorType.InvalidFileType), dir);
    }

    fs.mkdirSync(dir, { recursive: true });
    callback(null, dir);
  },
  filename(req, file, callback) {
    const fileId = uuidv4();

    callback(null, `${fileId}`);
  }
});