import { isDate } from 'util/types';
import { DateComparison } from '../shared/enums/date-comparison.enum';

class ValidationHelper {
  checkPropertiesNullOrEmpty<T extends object>(obj: T, keys: string[]): boolean {
    for (let key of keys) {
      if (!obj.hasOwnProperty(key)) return true;

      if (!(obj as any)[key]) return true;
    }

    return false;
  }

  validateBigInt(value: any) {
    try {
      BigInt(value);
      return true;
    } catch {
      return false;
    }
  }

  validateNumber(value: any) {
    try {
      return Number(value) ? true : false;
    } catch {
      return false;
    }
  }

  validateDate(dateInput: any, comparison: DateComparison) {
    const date = new Date(dateInput);
    const isValid = isFinite(Number(date));

    if (comparison === DateComparison.OnlyValid) return isValid;
    else return isValid && this.isDateInFuture(date, comparison);
  }

  isDateInFuture(date: Date, comparison: DateComparison) {
    const now = new Date();
    const numDate = Number(new Date(date.toDateString())); // toDateString() cuts off time and leaves only the date
    const numNow = Number(new Date(now.toDateString())); // this is wanted behavior!

    switch (comparison) {
      case DateComparison.Future:
        return numDate > numNow;
      case DateComparison.TodayAndFuture:
        return numDate >= numNow;
      default:
        return false;
    }
  }

  parseBoolean(string: string | undefined) {
    return string === 'true' ? true : string === 'false' ? false : undefined;
  }
}

export default new ValidationHelper();
