import { ILike } from 'typeorm';

export class SearchHelper {
  public search(searchValue: string): { key: string; value: any }[][] {
    let resultArray: { key: string; value: any }[][] = [];
    const searchOption: string[] = ['Name', 'Description', 'Manufacturer'];

    for (let option of searchOption) {
      resultArray.push([{ key: `${option}`, value: ILike(`%${searchValue}%`) }]);
    }

    resultArray.push([{ key: 'Categories', value: { Category: { Name: ILike(`%${searchValue}%`) } } }]);
    return resultArray;
  }

  public userSearch(searchValue: string): { key: string; value: any }[][] {
    let resultArray: { key: string; value: any }[][] = [];
    const searchOption: string[] = ['Firstname', 'Lastname', 'Email', 'UserId'];
    for (let option of searchOption) {
      resultArray.push([{ key: `${option}`, value: ILike(`%${searchValue}%`) }]);
    }

    return resultArray;
  }

  public orderSearch(searchValue: string): { key: string; value: any }[][] {
    let resultArray: { key: string; value: any }[][] = [];

    if (this.isNumeric(searchValue))
      resultArray.push([{ key: 'OrderId', value: BigInt(searchValue) }]);

    const searchOption: string[] = ['Firstname', 'Lastname', 'Email', 'UserId'];
    for (let option of searchOption) {
      resultArray.push([{ key: 'User', value: { [option]: ILike(`%${searchValue}%`) } }]);
    }

    return resultArray;
  }

  private isNumeric(str: any): boolean {
    return !isNaN(str) && !isNaN(parseFloat(str));
  }
}
