import { Equal } from 'typeorm';
import { ProductListFilter } from '../shared/models/product-list-filter.model';
import { SearchHelper } from './search.helper';
import { UserListFilter } from '../shared/models/user-list-filter.model';
import { OrderFilterList } from '../shared/models/order-list-filter.model';

export class FilterOption {
  public filter(filterOptionsTyped: ProductListFilter) {
    let filterOrArray: { key: string; value: any }[][] = [];
    const filterAndArray: { key: string; value: any }[] = [];
    const filterRelationsOrArray: { key: string; value: any }[][] = [];
    let filterRelations: {
      key: string;
      value: { key: string; value: { key: string; value: any }[][] };
    }[] = [];

    if (filterOptionsTyped.search !== '') {
      const searchHelper = new SearchHelper();
      filterOrArray = searchHelper.search(filterOptionsTyped.search);
    }

    if (!filterOptionsTyped.isInternal) {
      filterAndArray.push({
        key: 'IsInternal',
        value: filterOptionsTyped.isInternal,
      });
    }

    if (filterOptionsTyped.manufacturer !== '') {
      filterAndArray.push({
        key: 'Manufacturer',
        value: filterOptionsTyped.manufacturer,
      });
    }

    if (filterOptionsTyped.categories.length > 0) {
      for (let i in filterOptionsTyped.categories) {
        //filterRelationsOrArray.push([{ key: 'Name', value: ILike(`%${filterOptionsTyped.categories[i]}%`) }]);
        filterRelationsOrArray.push([{ key: 'CategoryId', value: BigInt(filterOptionsTyped.categories[i]) }]);
      }

      filterRelations = [
        {
          key: 'Categories',
          value: { key: 'Category', value: filterRelationsOrArray },
        },
      ];
    }

    return {
      filterOrArray,
      filterAndArray,
      filterRelationsOrArray,
      filterRelations,
    };
  }

  public userFilter(filterOptionsTyped: UserListFilter) {
    let filterOrArray: { key: string; value: any }[][] = [];
    const filterAndArray: { key: string; value: any }[] = [];
    const filterRelationsOrArray: { key: string; value: any }[][] = [];
    let filterRelations: {
      key: string;
      value: { key: string; value: { key: string; value: any }[][] };
    }[] = [];

    if (filterOptionsTyped.search !== '') {
      const searchHelper = new SearchHelper();
      filterOrArray = searchHelper.userSearch(filterOptionsTyped.search);
    }

    if (filterOptionsTyped.hasRequestedRoleUpdate !== undefined) {
      filterAndArray.push({ key: 'HasRequestedRoleUpdate', value: filterOptionsTyped.hasRequestedRoleUpdate });
    }

    return {
      filterOrArray,
      filterAndArray,
      filterRelationsOrArray,
      filterRelations,
    };
  }

  public orderFilter(filterOptionsTyped: OrderFilterList) {
    let filterOrArray: { key: string; value: any }[][] = [];
    const filterAndArray: { key: string; value: any }[] = [];
    const filterRelationsOrArray: { key: string; value: any }[][] = [];
    let filterRelations: {
      key: string;
      value: { key: string; value: { key: string; value: any }[][] };
    }[] = [];

    if (filterOptionsTyped.search !== '') {
      const searchHelper = new SearchHelper();
      filterOrArray = searchHelper.orderSearch(filterOptionsTyped.search);
    }

    if (filterOptionsTyped.orderDate !== '') {
      filterAndArray.push({
        key: 'OrderDate',
        value: Equal(new Date(filterOptionsTyped.orderDate)),
      });
    }
    if (filterOptionsTyped.orderStatus != undefined) {
      filterAndArray.push({
        key: 'OrderStatus',
        value: Equal(filterOptionsTyped.orderStatus),
      });
    }

    return {
      filterOrArray,
      filterAndArray,
      filterRelationsOrArray,
      filterRelations,
    };
  }
}
