import { Response } from 'express';
import { HttpCode } from '../shared/enums/http-codes.enum';
import { ErrorType } from '../shared/enums/error-types.enum';

class ErrorHandler {
  handleControllerException(error: unknown): string {
    if (typeof error === 'string') {
      // handle string error
      console.log(`[ERROR:string]: ${error}\n`);
      return error;
    } else if (error instanceof Error) {
      // handle Error object
      console.log(`[ERROR:${error.name}]: ${error.message}\n[STACKTRACE]: ${error.stack}\n`);
      return error.message;
    } else {
      // handle other types of errors
      console.log(`[ERROR:unknown]: ${error}\n`);
      return 'Unknown error occured';
    }
  }

  handlePostValueError(value: any): string | undefined {
    if (Object.keys(value).length === 0) {
      return 'sent_body_empty';
    }
  }

  handleControllerListParameters(page: string, take: string): ErrorType | undefined {
    if (Number.isNaN(Number(page))) return ErrorType.PageParamIsNaN;

    if (Number.isNaN(Number(take))) return ErrorType.TakeParamIsNaN;

    if (Number(page) < 1) return ErrorType.PageParamOutOfBounds;

    if (Number(take) < 1) return ErrorType.TakeParamOutOfBounds;
  }

  handleControllerCancellation(res: Response, statusCode: HttpCode, msg: ErrorType) {
    return res.status(statusCode).json({ error: msg });
  }
}

export default new ErrorHandler();
