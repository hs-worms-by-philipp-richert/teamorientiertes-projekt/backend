printf "\nDon't add passphrase!\n\n"
ssh-keygen -t rsa -b 4096 -m PEM -f ./keys/jwtRS256.key
openssl rsa -in ./keys/jwtRS256.key -pubout -outform PEM -out ./keys/jwtRS256.key.pub
printf "\nDone! Saved in ./keys\n\n"