# Documentation

## Table of Contents
[[_TOC_]]


## Prerequisites
- Linux system (preferably Debian 12 or similar)
- Node.js installed (we used: 21.7.1)
- npm installed (we used: 10.5.0)
- Docker installed (we used: version 25.0.4, build 1a576c5)
- Docker Compose installed (we used: version v2.24.7)
- nginx installed
- system specs should be evaluated by an administrator, depending on how much traffic and stored data (database, images, and PDFs) is expected


## Installation for production
This project is split into two backend-applications. The main application is the current one *("backend")*, the secondary webserver/service *("backend-document-generator")* is dedicated for PDF generation and the sending of emails.

### Cloning
1. Clone the projects, it is recommended to clone with SSH:
    - `git clone git@gitlab.rlp.net:top/24s/davmat/backend.git`
    - `git clone git@gitlab.rlp.net:top/24s/davmat/backend-document-generator.git`

2. Cloning the shared repository

    Both of these repositories make use of the git submodules feature to include a shared repository that holds common interfaces and more. Go inside the directory with `cd backend` and execute the commands below. After that, repeat these commands inside the secondary service: `cd backend-document-generator`.
    1. `git submodule init`
    2. `git submodule update`

### Installing dependencies
1. `backend` repository: run the command `npm i`
2. `backend-document-generator` repository: run the command `npm i`

### Configuration
Both repositories have a `.env.dist` file that serves as a template for configuration. Copy this file with `cp .env.dist .env`, this will automatically name the copied file `.env`. Edit the files as described below:
1. `backend` repository
    - PORT: you can leave this port at `3000`
    - DB_HOST: you can leave the host as `localhost`, unless the database is on a different machine
    - DB_PORT: leave as `5432`
    - DB_USERNAME: has to match the username set in `docker-compose.yml`: `services.db.environment.POSTGRES_USER`
    - DB_PASSWORD: has to match the username set in `docker-compose.yml`: `services.db.environment.verySecurePassword`
    - DB_DATABASE: leave as `"rental_db"`
    - MONGODB_CONN: this connection string has to match the credentials set inside `docker-compose.yml`: `services.mongo.environment`
    - DOCUMENTSERVICE: this service is used internally, you can leave it as `"http://localhost:3001/"`
    - FRONTEND_ORIGIN: this **must** be the URL at which the user-interface will be available (without trailing-slash). If the website is available at https://example.com, this value should be `"https://example.com"`
    - NODE_ENV: change value to `"production"`

2. `backend/docker-compose.yml` file
    - adjust the credentials of the main-database (postgres) by editing the values at `services.db.environment`
    - adjust the credentials of the secondary database (mongodb) by editing the values at `services.mongo.environment`
    - > :paperclip: **Note:** the MongoDB database is used for task-scheduling. When running the container later-on, it should automatically execute the start-up shell script. If it doesn't, you need to go into the mongo container manually and do the steps from `mongo-init.sh` yourself. 


3. `backend-document-generator` repository
    - PORT: you can leave this port at `3001`
    - SMTPHOST: the SMTP domain of your email provider
    - SMTPPORT: ask your email provider which SMTP port they use
    - EMAILUSER: your email address
    - EMAILPASSWORD: your email password
    - NODE_ENV: change value to `"production"`

### Starting Docker containers
Make sure you have [docker](https://docs.docker.com/get-docker/) and [docker compose](https://docs.docker.com/compose/) installed. Then run the command `sudo docker compose -f docker-compose.yml up -d`. The `-d` parameter runs the container in a detached shell. If you want, you can leave out this parameter when starting for the first time to easily check for any errors.

### Changing the key-pair
You may have noticed the `keys` directory with a key-pair inside. This is used for the signing of our login-tokens. We *highly* recommend changing these tokens. We have prepared a script for this: `generate-jwt-keys.sh`. Execute the script and follow the prompts. You may have to delete the already existing keys beforehand.

### Build the project
This project was developed with TypeScript. To build it, simply run `npm run build` inside both projects. This will create a new directly called `dist`. You can now run `npm run serve` to run the application in production mode. Don't forget to do this step for *both* backend-applications (*backend* and *backend-document-generator*).

### Running applications for production
Starting the application like any other Node.js application is not very practical. That is why we recommend using [pm2](https://www.npmjs.com/package/pm2). A simple breakdown of pm2 usage is to use the command `pm2 start dist/index.js`. You can check the status of your applications with `pm2 list`. For more information on usage and configuration-possibilities, check the [documentation of pm2](https://www.npmjs.com/package/pm2?activeTab=readme#installing-pm2).

> :paperclip: **Note:** don't forget to run both applications (*backend* and *backend-document-generator*) with pm2!


## Preparing the database
After the above steps, the system itself should already be fully functional. There is just one little catch: if you've already set up the frontend, you might want to try and log in. And that's the problem... which user? There isn't one yet. Setting up the first admin user is exactly what we have to do in the following steps.

1. Log in to the database

    You may have noticed the docker image "Adminer" inside the compose-file. Adminer is a simple and light-weight database UI, which we recommend using. Head to [localhost:8080](http://localhost:8080) and type in the database credentials provided in step 2 of chapter *[Configuration](#Configuration)*. The "Server" field must stay as "db" and the "Database" field must be left empty. Don't forget to select "PostgreSQL" in the dropdown-menu.

2. Select "rental_db

3. Select the "role" table

4. Click on "New item" for add new roles **(read more below!)**

Roles are a little bit tricky at first. We thought about how we could do role-based clearance without hard-coding anything and having maximum configurability. Our solution: an enum with four values: `Member = 0, InternalMember = 1, WarehouseWorker = 2, Admin = 4`. For Linux users this might already seem familiar. Our roles are the sum of these enum-items, which then form octal-values.

It is important to note that the permission-level of an admin user, for example, will *not* be 4, but 7! The enum is only used to categorize certain actions inside our code. To allow an admin user all actions, he must have the permission-level of all permissions summed-up.

To make it easier, here's an example set-up of the roles:
- Name: Mitglied, Permission: 0
- Name: Mitarbeiter, Permission: 1
- Name: Lagermitarbeiter, Permission: 3
- Name: Admin, Permission: 7


## Customization
The appearance of the frontend can be configured on the frontend side, so it does not belong into this backend-documentation. However, our application can generate PDFs and send emails. This happens in the "backend-document-generator".

If you want to customize any of the PDFs or email-contents, you'll find the files inside `backend-document-generator/src/views`. These files are coded just like normal HTML and CSS, but thanks to EJS we can use variables to fill these pages with data.


## Section for developers

### Webserver backend
1. Install npm packages with `npm i`
2. Copy `.env.dist` and rename to `.env`
3. Check that values inside `.env` are correct
4. Initialize shared-submodule with `git submodule init` and `git submodule update`
5. Start node application with `npm run dev`

### Docker
```bash
# start
sudo docker compose -f docker-compose.yml up

# stop
sudo docker compose stop

```


## Authors and acknowledgment
- Batha, Marc-Esdras
- Braun, Leon
- Durczak, Bartosz
- Franz, Fabian
- Heda-Tankeu, Joel-Yvan
- Heimsoeth, Lewin
- Noubissi-Kala, Theodore
- Richert, Philipp
- Sen, Muhammed


## License
MIT License

Copyright (c) 2024 Alpen Goats


## Project status
This project is almost entirely finished and should be production-ready, however without guarantee. With only a very limited time-frame, we developed an impressively large application. And while we developed this application very professionally and to our best knowledge, it should still be treated as what it is: a project by students developed in only a matter of weeks. Any natural or legal person should have a professional review our code before going into production with it.
